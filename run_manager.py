#!/usr/bin/python3

import os
import subprocess
import glob
import sys
import re
import shutil
import glob
import struct
import zipfile

from system_setup.generate_base_tcl import *

###-> check that Vivado variables are on in session
#
if not ("Vivado" in os.environ['PATH']):
	print("\n\n\tYou need to run xilinx_env first\n\n")
	sys.exit(1)
#
###


####-> remove files
#
ip_regex = re.compile(".*zip")                                  # find zip files
ip_dirs = os.listdir('sources/ip')                               # get list of files and directories on ip dir
filtered = [i for i in ip_dirs if not ip_regex.search(i)]       # remove zip files from list

for i in filtered:                                              # delete non-zip files and directories
	shutil.rmtree('sources/ip/' + i)

for f in glob.glob("vivado*"):                                   # remove old logs
	os.remove(f)

if os.path.exists("prismatic_hbm_x.tcl"):
	os.remove("prismatic_hbm_x.tcl")

#shutil.rmtree('Prismatic_prj', True)                                  # remove previous Vivado project

#
###
ILA_ON = True

module_dir=os.getcwd()

input_list = open('input_list/list.csv')
next(input_list)

for row in input_list:
	if os.path.exists('Prismatic_prj'):
		shutil.rmtree('Prismatic_prj', True)
	row = row.rstrip()
	req_gen_rep, parse_rep, hash_rep, channel_rep, read_rep, write_rep, read_reg_rep, write_reg_rep, tcpBRAM_rep, tcpMEM_rep, mem_chl_rep, requests, words, r_mems, w_mems, b_mems, chan_mode = row.split(',')

	print("\n\n\n###########################################################################################################################")
	print("###########################################################################################################################\n\n\n")
	print('\t' + row)
	print("\n\n\n###########################################################################################################################")
	print("###########################################################################################################################\n\n\n")

	###-> Generate all the tcl scripts (instances, update_ip, properties, addresses)
	#

	generate_base_tcl( req_gen_rep, parse_rep, hash_rep, channel_rep, read_rep, write_rep, read_reg_rep, write_reg_rep, tcpBRAM_rep, tcpMEM_rep, mem_chl_rep, requests, words, r_mems, w_mems, b_mems, chan_mode )
	#
	###


	###-> Add ILA to design
	#
	os.chdir(module_dir)
	out_file = open('prismatic_hbm_x.tcl','a')
	with open("prismatic_hbm.tcl") as file_ip:
		for row in file_ip:
			if ILA_ON:
				new_row=re.sub('#(source output_tcl/ila.tcl)', r'\1', row)
			else:
				new_row=re.sub('(^source output_tcl/ila.tcl)', r'#\1', row)
			out_file.write(new_row)
	out_file.close()
	#
	###


	###-> Create current version of Prismatic
	#
	subprocess.run(['vivado','-mode','batch','-source','prismatic_hbm_x.tcl'])
	#
	######################################################################################################
#		###-> Add ILA to design
#		#
#		out_file = open('prismatic_hbm_x.tcl','a')
#		with open("prismatic_hbm.tcl") as file_ip:
#			for row in file_ip:
#				if ILA_ON:
#					new_row=re.sub('#(source output_tcl/ila.tcl)', r'\1', row)
#				else:
#					new_row=re.sub('(^source output_tcl/ila.tcl)', r'#\1', row)
#				out_file.write(new_row)
#		out_file.close()
#		#
#		###
#
#
#		###-> Create current version of Prismatic
#		#
#		subprocess.run(['vivado','-mode','batch','-source','prismatic_hbm_x.tcl'])
#		#
#		###
#
#
	###-> Remove design-generating script
	#
	os.remove('prismatic_hbm_x.tcl')
	#
	###

	###-> Create bitstream of current version
	#
	subprocess.run(['vivado','-mode','batch','-source','bitstream_gen.tcl'])
	#
	###
	prologue = req_gen_rep + '_' + parse_rep + '_' + hash_rep + '_' + channel_rep + '_' + read_rep + '_' + write_rep + '_' + read_reg_rep + '_' + write_reg_rep + '_' + tcpBRAM_rep + '_' + tcpMEM_rep + '_' + mem_chl_rep + '_' + requests + '_' + words + '_' + r_mems + '_' + w_mems + '_' + b_mems + '_' + chan_mode

	home = os.environ.get('HOME')
	project_base = 'projects/in_work/Prismatic_hbm-1.0'
	project_main = home + '/' + project_base + '/Prismatic_prj'
	project_imp = 'Prismatic_prj/Prismatic.runs/impl_1'
	bit_base = 'prismatic_hbm_design_wrapper'
	bit_src = home + '/' + project_base + '/' + project_imp + '/' + bit_base
	bit_dst = home + '/' + project_base + '/' + 'bitstreams' + '/' + bit_base + '_' + prologue

	print(bit_src)
	print(bit_dst)

	try:
		os.rename(project_main,'Done_projects/' + prologue)
	except OSError:
		print('Destination project already exists:')
		print('\t' + prologue + '\n')
		exit(1)
#	try:
#		os.rename(bit_src + '.bit', bit_dst + '.bit')
#	except OSError:
#		print('Destination bitstream already exists:')
#		print('\t' + bit_dst + '.bit')
#		exit(1)
#
#	try:
#		os.rename(bit_src + '.ltx', bit_dst + '.ltx')
#	except OSError:
#		print('Destination probe file already exists:')
#		print('\t' + bit_dst + '.ltx')
#		exit(1)

	os.chdir(module_dir)
#		###-> Run ILA
#		#
#		subprocess.run(['vivado','-mode','batch','-source','run_ila.tcl'])
#		#
#		###
#
#		with zipfile.ZipFile('my_hw_ila_data_file.zip') as myzip:
#			with myzip.open('waveform.csv') as myfile:
#				time_row = myfile.read()
#
#		time_no_format = time_row.decode("utf-8").split(',',6)
#
#		h = time_no_format[-1][0:-1]
#		cycles = struct.unpack('>i', bytes.fromhex(h))[0]
#
#		os.remove('my_hw_ila_data_file.zip')
#
#		stats_string = req_gen_rep + ','
#		stats_string += parse_rep + ','
#		stats_string += hash_rep + ','
#		stats_string += channel_rep + ','
#		stats_string += read_rep + ','
#		stats_string += write_rep + ','
#		stats_string += read_reg_rep + ','
#		stats_string += write_reg_rep + ','
#		stats_string += mem_chl_rep + ','
#		stats_string += requests + ','
#		stats_string += str(cycles) + '\n'
#
#		stats_file = open('output_stats.txt','a')
#		stats_file.write(stats_string)
#		stats_file.close()
