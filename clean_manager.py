#!/usr/bin/python3

import os
import subprocess
import glob
import sys
import re
import shutil
import glob
import struct
import zipfile

from system_setup.generate_base_tcl import *

###-> check that Vivado variables are on in session
#
if not ("Vivado" in os.environ['PATH']):
	print("\n\n\tYou need to run xilinx_env first\n\n")
	sys.exit(1)
#
###


####-> remove files
#
ip_regex = re.compile(".*zip")                                  # find zip files
ip_dirs = os.listdir('sources/ip')                               # get list of files and directories on ip dir
filtered = [i for i in ip_dirs if not ip_regex.search(i)]       # remove zip files from list

for i in filtered:                                              # delete non-zip files and directories
	shutil.rmtree('sources/ip/' + i)

for f in glob.glob("vivado*"):                                   # remove old logs
	os.remove(f)

shutil.rmtree('Prismatic_prj', True)                                  # remove previous Vivado project

