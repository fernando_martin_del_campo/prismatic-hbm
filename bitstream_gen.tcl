open_project Prismatic_prj/Prismatic.xpr
reset_run synth_1
launch_runs synth_1 -jobs 20
wait_on_run synth_1
reset_run impl_1
launch_runs impl_1 -jobs 20
wait_on_run impl_1

launch_runs impl_1 -to_step write_bitstream -jobs 20
wait_on_run impl_1


#reset_run prismatic_hbm_design_time_const_A_0_synth_1
#reset_run prismatic_hbm_design_counter_xor_c_0_synth_1
#launch_runs impl_1 -to_step write_bitstream -jobs 20
