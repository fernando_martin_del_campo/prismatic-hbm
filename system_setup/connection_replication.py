import re
import sys
from math import ceil

def rreplace(s, old, new, occurrence):
	li = s.rsplit(old, occurrence)
	return new.join(li)

def connection_replication( connection_file_in, connection_file_out, rep_arguments = [] ):
	i=2

	mod_list = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','V','W','X','Y','Z')

	regeX_rep = '>> rep'
	regeX_rep_hl = '>> rep_hl'
	regeX_rep_hh = '>> rep_hh'
	regeX_rep2l = '>> rep/2l'
	regeX_rep2h = '>> rep/2h'
	regeX_repM1 = '>> repM1'
	regeX_repM2 = '>> repM2'
	regeX_rep_p = '>> rep_p'
	regeX_2_rep = '>> rep2'
	regeX_3_rep = '>> rep3'
	regeX_Mul = '>> repMul'
	regeX_Mul2 = '>> repMul2'
	regeX_work = '>>'

	mod_rep = re.compile(regeX_rep)
	mod_rep_hl = re.compile(regeX_rep_hl)
	mod_rep_hh = re.compile(regeX_rep_hh)
	mod_rep2l = re.compile(regeX_rep2l)
	mod_rep2h = re.compile(regeX_rep2h)
	mod_rep_m1 = re.compile(regeX_repM1)
	mod_rep_m2 = re.compile(regeX_repM2)
	mod_rep_p = re.compile(regeX_rep_p)
	mod_2_rep = re.compile(regeX_2_rep)
	mod_3_rep = re.compile(regeX_3_rep)
	mod_mul = re.compile(regeX_Mul)
	mod_mul2 = re.compile(regeX_Mul2)
	mod_work = re.compile(regeX_work)

	replication=0
	rep_number=0
	sec_rep_number=0

	last_rep = "none"

	connection_out = open(connection_file_out,'w')

	with open(connection_file_in) as file_tcl:
		for row in file_tcl:
			if mod_3_rep.match(row):
				replication = row.split(' ',5)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				third_rep_number=int(replication[4])
				for i in range( rep_arguments[rep_number] ):
					for j in range( rep_arguments[sec_rep_number] ):
						for k in range( rep_arguments[third_rep_number] ):
							out_string = replication[5].replace('_X','_' + mod_list[i])
							out_string = out_string.replace('_Y','_' + str(j))
							out_string = out_string.replace('M0Y','M0' + str(j))
							out_string = out_string.replace('_Z','_' + str(k))
							out_string = out_string.replace('M0Z','M0' + str(k))
							connection_out.write(out_string)
			elif mod_2_rep.match(row):
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				for i in range( rep_arguments[rep_number] ):
					for j in range( rep_arguments[sec_rep_number] ):
						out_string = replication[4].replace('_X','_' + mod_list[i])
						out_string = out_string.replace('_Z','_' + mod_list[j])

						if rep_arguments[rep_number] > 1:
							out_string = out_string.replace('_Q','_' + str(i))
						else:
							out_string = out_string.replace('_Q','')

						if rep_arguments[sec_rep_number] > 1:
							out_string = out_string.replace('_Y','_' + str(j))
						else:
							out_string = out_string.replace('_Y','')

						out_string = out_string.replace('M0Y','M0' + str(j))

						connection_out.write(out_string)
			elif mod_rep_p.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] + 1 ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_rep_m1.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(i+1))
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(i+1))
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_rep_m2.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				j = 0
				for i in range( int(rep_arguments[rep_number]/2),rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(j+1))
						out_string = out_string.replace('_Z', '_' + str(j))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(j+1))
					j = j + 1
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_rep2l.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(i+1))
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(i+1))
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_rep2h.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2),rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(i+1))
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(i+1))
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_rep_hl.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(i+1))
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(i+1))
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_rep_hh.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2),rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(i+1))
						out_string = out_string.replace('_Z', '_' + mod_list[i - int(rep_arguments[rep_number]/2)])
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(i+1))
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_mul2.match(row):
				replication = row.split(' ',2)

				index_w = rep_arguments[-1]
				index_r = rep_arguments[-2]
				index_b = rep_arguments[-3]

				for i in range ( index_w ):
					out_string = replication[2].replace('_X','_' + 'A' + '_' + str(i))
					out_string = out_string.replace('0Z', "{:02}".format(i))
					connection_out.write(out_string)
				for i in range ( index_r ):
					out_string = replication[2].replace('_X','_' + 'B' + '_' + str(i))
					out_string = out_string.replace('0Z', "{:02}".format(index_w + i))
					connection_out.write(out_string)
				for i in range ( index_b ):
					out_string = replication[2].replace('_X','_' + 'C' + '_' + str(i))
					out_string = out_string.replace('0Z', "{:02}".format(index_w + index_r + i))
					connection_out.write(out_string)
			elif mod_mul.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( ceil(rep_arguments[rep_number]*(rep_arguments[-1]+rep_arguments[-2]+rep_arguments[-3])/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					connection_out.write(out_string)
			elif mod_rep.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('SW+','S' + format(1+i,'02d'))
					out_string = out_string.replace('SB+','S' + format(1+i+rep_arguments[-4],'02d'))
					out_string = out_string.replace('SR+','S' + format(1+i+2*rep_arguments[-4],'02d'))
					out_string = out_string.replace('SW','S' + format(i,'02d'))
					out_string = out_string.replace('SB','S' + format(i,'02d'))
					out_string = out_string.replace('SR','S' + format(i,'02d'))
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z+', '_' + str(i+1))
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z+', str(i+1))
					#pad_str="{:02}".format(i)
					out_string = out_string.replace('0Z', "{:02}".format(i))
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_work.match(row):
				replication = row.split(' ',2)
				rep_number = int(replication[1])
				out_string = replication[2].replace('Z', str(rep_arguments[rep_number]))
				connection_out.write(out_string)
			else:
				connection_out.write(row)
	file_tcl.closed
	connection_out.closed
	return
