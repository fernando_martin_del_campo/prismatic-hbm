import re
import sys

def rreplace(s, old, new, occurrence):
	li = s.rsplit(old, occurrence)
	return new.join(li)

def connection_replication( connection_file_in, connection_file_out, rep_arguments = [] ):
	i=2

	mod_list = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P')

	regeX_rep = '>> rep'
	regeX_2_rep = '>> rep2'
	regeX_3_rep = '>> rep3'
	regeX_work = '>>'
	regeX_X = 'X'

	mod_rep = re.compile(regeX_rep)
	mod_2_rep = re.compile(regeX_2_rep)
	mod_3_rep = re.compile(regeX_3_rep)
	mod_work = re.compile(regeX_work)
	mod_X = re.compile(regeX_X)

	replication=0
	rep_number=0
	sec_rep_number=0

	last_rep = "none"

	connection_out = open(connection_file_out,'w')

	with open(connection_file_in) as file_tcl:
		for row in file_tcl:
			if mod_3_rep.match(row):
				replication = row.split(' ',5)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				third_rep_number=int(replication[4])
				for i in range( rep_arguments[rep_number] ):
					for j in range( rep_arguments[sec_rep_number] ):
						for k in range( rep_arguments[third_rep_number] ):
							out_string = replication[5].replace('_X','_' + mod_list[i])
							out_string = out_string.replace('_Y','_' + str(j))
							out_string = out_string.replace('M0Y','M0' + str(j))
							out_string = out_string.replace('_Z','_' + str(k))
							out_string = out_string.replace('M0Z','M0' + str(k))
							connection_out.write(out_string)
			elif mod_2_rep.match(row):
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				for i in range( rep_arguments[rep_number] ):
					for j in range( rep_arguments[sec_rep_number] ):
						row_int = replication[4].split(' ',4)
						out_string_tmp = replication[4]
						if ( len(row_int) == 5):
							if mod_X.match(row_int[2]) == None:
								row_int[2] = row_int[2].replace('_Y','_' + mod_list[j])
								out_string_tmp = row_int[0] + ' ' + row_int[1] + ' ' + row_int[2] + ' ' + row_int[3] + ' ' + row_int[4]
							elif mod_X.match(row_int[4]) == None:
								row_int[4] = row_int[4].replace('_Y','_' + mod_list[j])
								out_string_tmp = row_int[0] + ' ' + row_int[1] + ' ' + row_int[2] + ' ' + row_int[3] + ' ' + row_int[4]
						out_string = out_string_tmp.replace('_X','_' + mod_list[i])
						out_string = out_string.replace('_Y','_' + str(j))
						out_string = out_string.replace('_Z','_' + mod_list[j])
						out_string = out_string.replace('M0Y','M0' + str(j))
						connection_out.write(out_string)
			elif mod_rep.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					if ( rep_arguments[rep_number] > 1 ):
						out_string = out_string.replace('_Z', '_' + str(i))
					else:
						out_string = out_string.replace('_Z', '')
					out_string = out_string.replace('Z', str(i))
					connection_out.write(out_string)
			elif mod_work.match(row):
				replication = row.split(' ',2)
				rep_number = int(replication[1])
				out_string = replication[2].replace('Z', str(rep_arguments[rep_number]))
				connection_out.write(out_string)
			else:
				connection_out.write(row)
	file_tcl.closed
	connection_out.closed
	return
