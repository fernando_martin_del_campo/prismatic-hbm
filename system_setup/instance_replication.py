import re
import sys
from math import ceil

def rreplace(s, old, new, occurrence):
	li = s.rsplit(old, occurrence)
	return new.join(li)

def instance_replication( instance_file_in, instance_file_out, rep_arguments = [] ):
	i=2

	mod_list = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','V','W','X','Y','Z')

	regeX_rep = '>> rep'
	regeX_rep_h = '>> rep/2'
	regeX_rep_p = '>> rep_p'
	regeX_2_rep = '>> rep2'
	regeX_Mul = '>> repMul'
	regeX_Mul2 = '>> repMul2'
	regeX_work = '>>'

	mod_rep = re.compile(regeX_rep)
	mod_rep_h = re.compile(regeX_rep_h)
	mod_rep_p = re.compile(regeX_rep_p)
	mod_2_rep = re.compile(regeX_2_rep)
	mod_mul = re.compile(regeX_Mul)
	mod_mul2 = re.compile(regeX_Mul2)
	mod_work = re.compile(regeX_work)

	replication=0
	rep_number=0
	sec_rep_number=0

	last_rep = "none"

	instance_out = open(instance_file_out,'w')

	no_chan = rep_arguments[8]

	with open(instance_file_in) as file_tcl:
		for row in file_tcl:
			if mod_2_rep.match(row):
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				for i in range( rep_arguments[rep_number] ):
					for j in range( rep_arguments[sec_rep_number] ):
						out_string = replication[4].replace('_X','_' + mod_list[i])
						if rep_arguments[sec_rep_number] > 1:
							out_string = out_string.replace('_Y','_' + str(j))
						else:
							out_string = out_string.replace('_Y','')
						instance_out.write(out_string)
			elif mod_rep_p.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] + 1 ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					instance_out.write(out_string)
			elif mod_rep_h.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					instance_out.write(out_string)
			elif mod_mul2.match(row):
				replication = row.split(' ',2)
				for i in range( rep_arguments[-3] ):
					out_string = replication[2].replace('_X','_' + 'A' + '_' + str(i))
					instance_out.write(out_string)
				for i in range( rep_arguments[-2] ):
					out_string = replication[2].replace('_X','_' + 'B' + '_' + str(i))
					instance_out.write(out_string)
				for i in range( rep_arguments[-1] ):
					out_string = replication[2].replace('_X','_' + 'C' + '_' + str(i))
					instance_out.write(out_string)
			elif mod_mul.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( ceil(rep_arguments[rep_number]*(rep_arguments[-1]+rep_arguments[-2]+rep_arguments[-3])/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					instance_out.write(out_string)
			elif mod_rep.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					instance_out.write(out_string)
			elif mod_work.match(row):
				replication = row.split(' ',2)
				rep_number = int(replication[1])
				out_string = replication[2].replace('Z', str(rep_arguments[rep_number]))
				instance_out.write(out_string)

			else:
				instance_out.write(row)
	file_tcl.closed
	instance_out.closed
	return
