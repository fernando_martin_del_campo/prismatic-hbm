import re
import sys
from math import log2
from math import ceil
from math import floor

def rreplace(s, old, new, occurrence):
	li = s.rsplit(old, occurrence)
	return new.join(li)

def properties_replication( properties_file_in, properties_file_out, mem_base, bram_base, mem_offset, bram_offset, requests, m_channels, r_mems, w_mems, b_mems, chan_mode, rep_arguments = [] ):
	i=2

	mod_list = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P')

	regeX_rep = '>> rep'
	regeX_rep_hl = '>> rep_hl'
	regeX_rep_hh = '>> rep_hh'
	regeX_2_rep = '>> rep2'
	regeX_plus = '>> rep_plus'
	regeX_less = '>> rep_less'
	regeX_less_h = '>> rep_less_h'
	regeX_work = '>>'
	regeX_mem = '.*MEMORY_BASE'
	regeX_channel = '.*SAXI'
	regeX_cnst_xor = '.*counter_xor_c'
	regeX_cnst_reqs = '.*time_const_'
	regeX_cnst_words = '.*num_channels_c'
	regeX_sum = '.*XYZ'
	regeX_sum_p = '.*XYZ\+'
	regeX_write = '.*SW\+'
	regeX_read = '.*SR\+'
	regeX_big = '.*SB\+'
	regeX_BRAMw = '.*repBRAMw'
	regeX_BRAMb = '.*repBRAMb'
	regeX_BRAMr = '.*repBRAMr'
	regeX_Mul2 = '>> repMul2'

	mod_rep = re.compile(regeX_rep)
	mod_plus = re.compile(regeX_plus)
	mod_less = re.compile(regeX_less)
	mod_less_h = re.compile(regeX_less_h)
	mod_rep_hl = re.compile(regeX_rep_hl)
	mod_rep_hh = re.compile(regeX_rep_hh)
	mod_2_rep = re.compile(regeX_2_rep)
	mod_work = re.compile(regeX_work)
	mod_mem = re.compile(regeX_mem)
	mod_channel = re.compile(regeX_channel)
	mod_cnst_xor = re.compile(regeX_cnst_xor)
	mod_cnst_reqs = re.compile(regeX_cnst_reqs)
	mod_cnst_words = re.compile(regeX_cnst_words)
	mod_sum = re.compile(regeX_sum)
	mod_sum_p = re.compile(regeX_sum_p)
	mod_write = re.compile(regeX_write)
	mod_read = re.compile(regeX_read)
	mod_big = re.compile(regeX_big)
	mod_BRAMw = re.compile(regeX_BRAMw)
	mod_BRAMb = re.compile(regeX_BRAMb)
	mod_BRAMr = re.compile(regeX_BRAMr)
	mod_mul2 = re.compile(regeX_Mul2)

	replication=0
	rep_number=0
	sec_rep_number=0

	last_rep = "none"
	cur_mem = 0

	properties_out = open(properties_file_out,'w')

	with open(properties_file_in) as file_tcl:
		for row in file_tcl:
			if mod_sum_p.match(row):
				#out_string = row.replace('XYZ+',str(m_channels*(r_mems + w_mems + b_mems)))
				out_string = row.replace('XYZ+',str(r_mems + w_mems + b_mems))
				properties_out.write(out_string)
			elif mod_sum.match(row):
				out_string = row.replace('XYZ',str(r_mems + w_mems + b_mems))
				properties_out.write(out_string)
			elif mod_2_rep.match(row):
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				if rep_arguments[sec_rep_number] != 1:
					out_string_tmp=replication[4].replace('#',str(rep_arguments[sec_rep_number]-1))
				else:
					out_string_tmp=replication[4].replace('#','1')
				k = 0

				for i in range( rep_arguments[rep_number] ):
					for j in range( rep_arguments[sec_rep_number] ):
						out_string = out_string_tmp.replace('_X','_' + mod_list[i])
						out_string = out_string.replace('_Y_',str(j))
						if chan_mode == 0:
							k = k + 1
							l = m_channels
						else:
							k = j + 1
							l = 1;
						out_string = out_string.replace('Z+',str(k))
						out_string = out_string.replace('Z',str(l))
						if rep_arguments[sec_rep_number] > 1:
							out_string = out_string.replace('_Y','_' + str(j))
						else:
							out_string = out_string.replace('_Y','')
						properties_out.write(out_string)
			elif mod_mul2.match(row):
				replication = row.split(' ',2)

				for i in range( rep_arguments[-1] ):
					out_string = replication[2].replace( '_X','_' + 'A' + '_' + str(i) )
					properties_out.write(out_string)
				for i in range( rep_arguments[-2] ):
					out_string = replication[2].replace( '_X','_' + 'B' + '_' + str(i) )
					properties_out.write(out_string)
				for i in range( rep_arguments[-3] ):
					out_string = replication[2].replace( '_X','_' + 'C' + '_' + str(i) )
					properties_out.write(out_string)
			elif mod_rep_hl.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2) ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('{Z', '{' + str(i))
					if mod_mem.match(out_string):
						out_string = out_string.replace('MEMORY_BASE','0x' + format(mem_base,'08X'))
						if chan_mode != 0:
							mem_base = mem_base + int(mem_offset)
					if mod_cnst_reqs.match(row):
						out_string = out_string.replace('cnst_val',str( requests ))
					if mod_cnst_words.match(row):
						out_string = out_string.replace('cnst_val_2',str( m_channels ))
					properties_out.write(out_string)
			elif mod_rep_hh.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2),rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('{Z', '{' + str(i - int(rep_arguments[rep_number]/2) ))
					if mod_mem.match(out_string):
						out_string = out_string.replace('MEMORY_BASE','0x' + format(mem_base,'08X'))
						if chan_mode != 0:
							mem_base = mem_base + int(mem_offset)
					if mod_cnst_reqs.match(row):
						out_string = out_string.replace('cnst_val',str( requests ))
					if mod_cnst_words.match(row):
						out_string = out_string.replace('cnst_val_2',str( m_channels ))
					properties_out.write(out_string)
			elif mod_BRAMw.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				cur_mem = bram_base
				step = rep_arguments[13] / rep_arguments[rep_number]
				if step > 1:
					step = 1
				mult = 0
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('BASE_BRAM','0x' + format(cur_mem,'08X'))
					properties_out.write(out_string)
					mult += step
					cur_mem = bram_base + bram_offset * floor(mult)
					#cur_mem = bram_base + bram_offset * ceil(mult)
			elif mod_BRAMb.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				#cur_mem = bram_base + bram_offset*rep_arguments[-1]
				bram_base_tmp = cur_mem
				step = rep_arguments[11] / rep_arguments[rep_number]
				if step > 1:
					step = 1
				mult = 0
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('BASE_BRAM','0x' + format(cur_mem,'08X'))
					properties_out.write(out_string)
					mult += step
					cur_mem = bram_base_tmp + bram_offset * floor(mult)
			elif mod_BRAMr.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				#cur_mem = bram_base + bram_offset*(rep_arguments[-1] + rep_arguments[-2])
				#cur_mem = bram_base + bram_offset*(rep_arguments[13] + rep_arguments[11])
				bram_base_tmp = cur_mem
				step = rep_arguments[11] / rep_arguments[rep_number]
				if step > 1:
					step = 1
				mult = 0
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('BASE_BRAM','0x' + format(cur_mem,'08X'))
					properties_out.write(out_string)
					mult += step
					cur_mem = bram_base_tmp + bram_offset * floor(mult)
				#input("Press Enter to continue...")
			elif mod_less_h.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2)+1 ):
					out_string = replication[3].replace('0Z', "{:02}".format(i))
					properties_out.write(out_string)
			elif mod_less.match(row):
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number]):
					out_string = replication[3].replace('0Z', "{:02}".format(i))
					properties_out.write(out_string)
			elif mod_plus.match(row):
				replication = row.split(' ',2)
				for i in range( r_mems + w_mems + b_mems ):
					out_string = replication[2].replace('0Z', "{:02}".format(i))
					properties_out.write(out_string)
			elif mod_rep.match(row):
				if chan_mode  == 0:
					cnst_val_2_tmp = 0
				else:
					cnst_val_2_tmp = m_channels
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] ):
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('{Z', '{' + str(i))
					out_string = out_string.replace('SW',str(i))
					if mod_mem.match(out_string):
						out_string = out_string.replace('MEMORY_BASE','0x' + format(mem_base,'08X'))
						mem_base = mem_base + int(mem_offset)
					if mod_cnst_reqs.match(row):
						out_string = out_string.replace('cnst_val',str( requests ))
					if mod_cnst_words.match(row):
						out_string = out_string.replace('cnst_val_2',str( cnst_val_2_tmp ))
					properties_out.write(out_string)
			elif mod_work.match(row):
				replication = row.split(' ',2)
				rep_number = int(replication[1])
				out_string = replication[2].replace('{Z/2+','{' + str(int(rep_arguments[rep_number]/2)+1))
				out_string = out_string.replace('{Z/2','{' + str(int(rep_arguments[rep_number]/2)))
				out_string = out_string.replace('{ZM','{' + str(int(rep_arguments[rep_number]/m_channels)))
				out_string = out_string.replace('{Z+','{' + str(rep_arguments[rep_number]+1))
				out_string = out_string.replace('{Z','{' + str(rep_arguments[rep_number]))
				properties_out.write(out_string)
			elif mod_channel.match(row):
				chan_no = int(row[17:19])
				if ( chan_no < rep_arguments[-4] ):
					out_string = row.replace('false','TRUE')
				else:
					out_string = row
				properties_out.write(out_string)
			elif mod_cnst_xor.match(row):
				out_string = row.replace('cnst_val',str( (requests*2) + 1 ))
				properties_out.write(out_string)
			elif mod_cnst_reqs.match(row):
				out_string = row.replace('cnst_val',str(requests))
				properties_out.write(out_string)
			else:
				properties_out.write(row)
	file_tcl.closed
	properties_out.closed
	return
