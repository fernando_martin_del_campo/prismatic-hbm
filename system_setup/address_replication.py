import re
import sys
from math import ceil

def rreplace(s, old, new, occurrence):
	li = s.rsplit(old, occurrence)
	return new.join(li)

def address_replication( address_file_in, address_file_out, read_base, write_base, mem_base, tcp_base, bram_base, read_offset, write_offset, mem_offset, tcp_offset, rep_arguments = [] ):
	i=2

	mod_list = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P')

	regeX_rep = '>> rep'
	regeX_rep_p = '>> rep_p'
	regeX_2_rep = '>> rep2'
	regeX_2_rep_lh = '>> rep2lh'
	regeX_2_rep_uh = '>> rep2uh'
	regeX_2_rep_p = '>> rep2_p'
	regeX_3_rep = '>> rep3'
	regeX_work = '>>'
	regeX_Mul = '>> repMul'
	regeX_Mul2 = '>> repMul2'

	regeX_RB = '.*BASE_R'
	regeX_BB = '.*BASE_B'
	regeX_WB = '.*BASE_W'
	regeX_MB = '.*BASE_M'
	regeX_TCP = '.*BASE_TCP'

	mod_rep = re.compile(regeX_rep)
	mod_rep_p = re.compile(regeX_rep_p)
	mod_2_rep = re.compile(regeX_2_rep)
	mod_2_rep_lh = re.compile(regeX_2_rep_lh)
	mod_2_rep_uh = re.compile(regeX_2_rep_uh)
	mod_2_rep_p = re.compile(regeX_2_rep_p)
	mod_3_rep = re.compile(regeX_3_rep)
	mod_mul = re.compile(regeX_Mul)
	mod_mul2 = re.compile(regeX_Mul2)
	mod_work = re.compile(regeX_work)

	mod_RB = re.compile(regeX_RB)
	mod_BB = re.compile(regeX_BB)
	mod_WB = re.compile(regeX_WB)
	mod_MB = re.compile(regeX_MB)
	mod_TCP = re.compile(regeX_TCP)

	replication=0
	rep_number=0
	sec_rep_number=0

	last_rep = "none"

	address_out = open(address_file_out,'w')

	with open(address_file_in) as file_tcl:
		for row in file_tcl:
			if mod_3_rep.match(row):
				replication = row.split(' ',5)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				third_rep_number=int(replication[4])
				for i in range( rep_arguments[rep_number] ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					for j in range( rep_arguments[sec_rep_number] ):
						for k in range( rep_arguments[third_rep_number] ):
							out_string = replication[5].replace('_X','_' + mod_list[i])
							out_string = out_string.replace('_Y','_' + str(j))
							out_string = out_string.replace('M0Y','M0' + str(j))
							out_string = out_string.replace('_Z','_' + str(k))
							out_string = out_string.replace('M0Z','M0' + str(k))
							if ( mod_RB.match(out_string)):
								out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
								read_base_tmp += read_offset
								out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
							elif ( mod_WB.match(out_string)):
								out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
								write_base_tmp += write_offset
								out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
							elif ( mod_MB.match(out_string)):
								out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
								out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
							address_out.write(out_string)
			elif mod_2_rep_p.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				for i in range( rep_arguments[rep_number] ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					tcp_base_tmp = tcp_base
					for j in range( rep_arguments[sec_rep_number] + 1):
						out_string = replication[4].replace('_X','_' + mod_list[i])
						out_string = out_string.replace('_Y','_' + mod_list[j])
						out_string = out_string.replace('Z', str(i))
						if ( mod_RB.match(out_string)):
							out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
							read_base_tmp += read_offset
							out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
						elif ( mod_WB.match(out_string)):
							out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
							write_base_tmp += write_offset
							out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
						elif ( mod_TCP.match(out_string)):
							out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
							tcp_base_tmp += tcp_offset
							out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
						elif ( mod_MB.match(out_string)):
							out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
							out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
						address_out.write(out_string)
						mem_base_tmp += mem_offset
			elif mod_2_rep_lh.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				for i in range( int(rep_arguments[rep_number]/2) ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					tcp_base_tmp = tcp_base
					for j in range( rep_arguments[sec_rep_number] ):
						out_string = replication[4].replace('_X','_' + mod_list[i])
						out_string = out_string.replace('_Y','_' + str(j))
						out_string = out_string.replace('_Z', '_' + mod_list[j])
						out_string = out_string.replace('Z', str(i))
						if ( mod_RB.match(out_string)):
							out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
							read_base_tmp += read_offset
							out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
						elif ( mod_WB.match(out_string)):
							out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
							write_base_tmp += write_offset
							out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
						elif ( mod_TCP.match(out_string)):
							out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
							tcp_base_tmp += tcp_offset
							out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
						elif ( mod_MB.match(out_string)):
							out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
							out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
						address_out.write(out_string)
						mem_base_tmp += mem_offset
			elif mod_2_rep_uh.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( int(rep_arguments[rep_number]/2),rep_arguments[rep_number] ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					tcp_base_tmp = tcp_base

					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('Z', str(i))
					if ( mod_RB.match(out_string)):
						out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
						read_base_tmp += read_offset
						out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
					elif ( mod_WB.match(out_string)):
						out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
						write_base_tmp += write_offset
						out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
					elif ( mod_TCP.match(out_string)):
						out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
						tcp_base_tmp += tcp_offset
						out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
					elif ( mod_MB.match(out_string)):
						out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
						out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
					address_out.write(out_string)
					mem_base_tmp += mem_offset
			elif mod_2_rep.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',4)
				rep_number=int(replication[2])
				sec_rep_number=int(replication[3])
				for i in range( rep_arguments[rep_number] ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					tcp_base_tmp = tcp_base
					for j in range( rep_arguments[sec_rep_number] ):
						out_string = replication[4].replace('_X','_' + mod_list[i])
						out_string = out_string.replace('_Y','_' + str(j))
						out_string = out_string.replace('_Z', '_' + mod_list[j])
						out_string = out_string.replace('0Z', "{:02}".format(i))
						out_string = out_string.replace('Z', str(i))
						if ( mod_RB.match(out_string)):
							out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
							read_base_tmp += read_offset
							out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
						elif ( mod_WB.match(out_string)):
							out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
							write_base_tmp += write_offset
							out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
						elif ( mod_TCP.match(out_string)):
							out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
							tcp_base_tmp += tcp_offset
							out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
						elif ( mod_MB.match(out_string)):
							out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
							out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
						address_out.write(out_string)
						mem_base_tmp += mem_offset
			elif mod_rep_p.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				write_base_tmp = write_base
				tcp_base_tmp = tcp_base
				for i in range( rep_arguments[rep_number] ):
					read_base_tmp = read_base
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('Z', str(i))
					if ( mod_RB.match(out_string)):
						out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
						read_base_tmp += read_offset
						out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
					elif ( mod_WB.match(out_string)):
						out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
						write_base_tmp += write_offset
						out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
					elif ( mod_TCP.match(out_string)):
						out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
						tcp_base_tmp += tcp_offset
						out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
					elif ( mod_MB.match(out_string)):
						out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
						out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
					address_out.write(out_string)
					mem_base_tmp += mem_offset
			elif mod_mul2.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',2)
				bram_base_tmp = bram_base
				for i in range(3):
					for j in range( rep_arguments[-1] ):
						read_base_tmp = read_base
						write_base_tmp = write_base
						tcp_base_tmp = tcp_base
						out_string = replication[2].replace('_X','_' + mod_list[i] + '_' + str(j))
						out_string = out_string.replace('0Z', "{:02}".format(i))
						out_string = out_string.replace('Z', str(i))
						if ( mod_BB.match(out_string)):
							out_string = out_string.replace('BASE_BRAM','0x'+format(bram_base_tmp,'08x'))
							bram_base_tmp += write_offset
							out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
						if ( mod_RB.match(out_string)):
							out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
							read_base_tmp += read_offset
							out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
						elif ( mod_WB.match(out_string)):
							out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
							write_base_tmp += write_offset
							out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
						elif ( mod_TCP.match(out_string)):
							out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
							tcp_base_tmp += tcp_offset
							out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
						elif ( mod_MB.match(out_string)):
							out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
							out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
						address_out.write(out_string)
						mem_base_tmp += mem_offset
			elif mod_mul.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				bram_base_tmp = bram_base
				for i in range( ceil(rep_arguments[rep_number]*(rep_arguments[-1]+rep_arguments[-2]+rep_arguments[-3])/2) ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					tcp_base_tmp = tcp_base
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('0Z', "{:02}".format(i))
					out_string = out_string.replace('Z', str(i))
					if ( mod_BB.match(out_string)):
						out_string = out_string.replace('BASE_BRAM','0x'+format(bram_base_tmp,'08x'))
						bram_base_tmp += write_offset
						out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
					if ( mod_RB.match(out_string)):
						out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
						read_base_tmp += read_offset
						out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
					elif ( mod_WB.match(out_string)):
						out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
						write_base_tmp += write_offset
						out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
					elif ( mod_TCP.match(out_string)):
						out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
						tcp_base_tmp += tcp_offset
						out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
					elif ( mod_MB.match(out_string)):
						out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
						out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
					address_out.write(out_string)
					mem_base_tmp += mem_offset
			elif mod_rep.match(row):
				mem_base_tmp = mem_base
				replication = row.split(' ',3)
				rep_number=int(replication[2])
				for i in range( rep_arguments[rep_number] ):
					read_base_tmp = read_base
					write_base_tmp = write_base
					tcp_base_tmp = tcp_base
					out_string = replication[3].replace('_X','_' + mod_list[i])
					out_string = out_string.replace('0Z', "{:02}".format(i))
					out_string = out_string.replace('Z', str(i))
					if ( mod_RB.match(out_string)):
						out_string = out_string.replace('BASE_R','0x'+format(read_base_tmp,'08x'))
						read_base_tmp += read_offset
						out_string = out_string.replace('RANGE_R','0x'+format(read_offset,'08x'))
					elif ( mod_WB.match(out_string)):
						out_string = out_string.replace('BASE_W','0x'+format(write_base_tmp,'08x'))
						write_base_tmp += write_offset
						out_string = out_string.replace('RANGE_W','0x'+format(write_offset,'08x'))
					elif ( mod_TCP.match(out_string)):
						out_string = out_string.replace('BASE_TCP','0x'+format(tcp_base_tmp,'08x'))
						tcp_base_tmp += tcp_offset
						out_string = out_string.replace('RANGE_TCP','0x'+format(tcp_offset,'08x'))
					elif ( mod_MB.match(out_string)):
						out_string = out_string.replace('BASE_M','0x'+format(mem_base_tmp,'08X'))
						out_string = out_string.replace('RANGE_M','0x'+format(mem_offset,'08X'))
					address_out.write(out_string)
					mem_base_tmp += mem_offset
			elif mod_work.match(row):
				replication = row.split(' ',2)
				rep_number = int(replication[1])
				out_string = replication[2].replace('Z', str(rep_arguments[rep_number]))
				address_out.write(out_string)

			else:
				address_out.write(row)
	file_tcl.closed
	address_out.closed
	return
