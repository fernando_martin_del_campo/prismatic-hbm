#!/usr/bin/python3

import re
import os
import subprocess
import glob
from system_setup.instance_replication import *
from system_setup.properties_replication import *
from system_setup.connection_replication import *
from system_setup.address_replication import *

from shutil import copy,rmtree
from filecmp import cmp

def run_vivado( version, vivado_cmd ):
	XILINX_HOME="/opt/Xilinx/Vivado/"
	if version == 2018:
		XILINX_HOME = XILINX_HOME + "2018.3"
	else:
		XILINX_HOME = XILINX_HOME + "2019.1"

	os.system("bash -c 'source " + XILINX_HOME + "/settings64.sh && " + vivado_cmd + "'")

def adjust_module(regex_prev, regex_post, rep_prev, rep_post, module_dir, ip_dest_dir, v_version):
	regex_prev_c = re.compile(regex_prev)
	regex_post_c = re.compile(regex_post)

	config_file = module_dir + "length_config.h"
	config_org_file = module_dir + "org.length_config.h"

	if ( os.path.isfile(config_file) ):
		os.remove(config_file)

	config_file_ptr = open(config_file,'a')

	with open(module_dir + "org.length_config.h") as file_ip:
		for row in file_ip:
			if regex_prev_c.match(row):
				cad_cmp=""
				cad = row.split()
				for word in cad[3:]:
					cad_cmp += " " + word
				cad_res = cad[0] + " " + cad[1] + "\t" + str(rep_prev) + cad_cmp + '\n'
			elif regex_post_c.match(row):
				cad_cmp=""
				cad = row.split()
				for word in cad[3:]:
					cad_cmp += " " + word
				cad_res = cad[0] + " " + cad[1] + "\t" + str(rep_post) + cad_cmp + '\n'
			else:
				cad_res = row
			config_file_ptr.write(cad_res)
	config_file_ptr.close()
	file_ip.close()

	#if ( cmp( config_file, config_org_file) ):
	#return
	#else:
	copy( module_dir + "length_config.h", module_dir + "org.length_config.h" )
	os.chdir(module_dir)
	if ( os.path.isdir(module_dir + "module_prj") ):
		rmtree(module_dir + "module_prj")
	if ( os.path.isfile("vivado_hls.log") ):
		os.remove("vivado_hls.log")
	#subprocess.run(["vivado_hls","-f","build_module.tcl"])
	run_viv_str="vivado_hls -f build_module.tcl"
	run_vivado( v_version, run_viv_str )

	for ip_zip in glob.glob('module_prj/solution1/impl/ip/xilinx_com_hls_*.zip'):
		copy(module_dir + ip_zip, ip_dest_dir)

	return

##############################################################
def generate_base_tcl( req_gen_rep_s, parse_rep_s, hash_rep_s, channel_rep_s, read_rep_s, write_rep_s, read_reg_rep_s, write_reg_rep_s, tcpBRAM_rep_s, tcpMEM_rep_s, mem_chl_rep_s, requests_s, words_s, r_mems_rep_s, w_mems_rep_s, b_mems_rep_s, chan_mode_s):
	req_gen_rep = int(req_gen_rep_s)
	parse_rep = int(parse_rep_s)
	hash_rep = int(hash_rep_s)
	channel_rep = int(channel_rep_s)
	read_rep = int(read_rep_s)
	write_rep = int(write_rep_s)
	read_reg_rep = int(read_reg_rep_s)
	write_reg_rep = int(write_reg_rep_s)
	mem_chl_rep = int(mem_chl_rep_s)
	tcpBRAM_rep = int(tcpBRAM_rep_s)
	tcpMEM_rep = int(tcpMEM_rep_s)
	requests = int(requests_s)
	words = int(words_s)
	r_mems_rep = int(r_mems_rep_s)
	w_mems_rep = int(w_mems_rep_s)
	b_mems_rep = int(b_mems_rep_s)
	chan_mode = int(chan_mode_s)


	CURRENT_DIR = os.getcwd()
	SOURCES_DIR = CURRENT_DIR + "/sources/base_tcl/"
	OUTPUT_DIR = CURRENT_DIR + "/output_tcl/"
	IP_SOURCE_DIR = os.environ['HOME'] + "/projects/modules/"
	IP_DEST_DIR = CURRENT_DIR + "/sources/ip"

	parser_arb_dir = IP_SOURCE_DIR + "parser_arbiter/"
	register_arbiter_dir = IP_SOURCE_DIR + "register_arbiter/"
	parser_dir = IP_SOURCE_DIR + "parser/"
	tcpIn_dir = IP_SOURCE_DIR + "tcp_in/tcp_in/"

	mem_offset = 0x10000000
	mem_offset_n = 0x10000000
	read_reg_offset = 0x2000
	write_reg_offset = 0x2000
	bram_offset = 0x2000
	tcp_offset = 0x2000

	mem_base = 0x00000000
	write_base = 0
	read_base = 0
	tcp_base = 0xC0000000
	bram_base = 0xC0002000

	rep_arguments = [req_gen_rep, parse_rep, hash_rep, channel_rep, read_rep, write_rep, read_reg_rep, write_reg_rep, tcpBRAM_rep, tcpMEM_rep, mem_chl_rep, b_mems_rep, r_mems_rep, w_mems_rep]

	if ( os.path.isdir(OUTPUT_DIR) ):
		rmtree(OUTPUT_DIR)
	os.mkdir(OUTPUT_DIR)

	instance_replication( SOURCES_DIR + "instances.tcl", OUTPUT_DIR + "instances.tcl", rep_arguments )

	properties_replication( SOURCES_DIR + "properties.tcl", OUTPUT_DIR + "properties.tcl", mem_base, bram_base, mem_offset_n, bram_offset, requests, mem_chl_rep, r_mems_rep, w_mems_rep, b_mems_rep, chan_mode, rep_arguments)
	connection_replication( SOURCES_DIR + "connections.tcl", OUTPUT_DIR + "connections.tcl", rep_arguments )
	address_replication( SOURCES_DIR + "addresses.tcl", OUTPUT_DIR + "addresses.tcl", read_base, write_base, mem_base, tcp_base, bram_base, read_reg_offset, write_reg_offset, mem_offset, tcp_offset, rep_arguments)

	copy(SOURCES_DIR + "preamble.tcl",OUTPUT_DIR)
	copy(SOURCES_DIR + "update_ip.tcl",OUTPUT_DIR)
	copy(SOURCES_DIR + "clocking.tcl",OUTPUT_DIR)
	copy(SOURCES_DIR + "ila.tcl",OUTPUT_DIR)

	adjust_module('.*REQ_MODULES', '.*PARSE.*_MODULES', req_gen_rep, parse_rep, parser_arb_dir, IP_DEST_DIR,2018)
	adjust_module('.*PARSE.*_MODULES', '.*REG_MODULES', parse_rep, read_rep, register_arbiter_dir, IP_DEST_DIR, 2018)
	adjust_module('.*M_CHANNELS', '.*M_CHANNELS', mem_chl_rep, mem_chl_rep, parser_dir, IP_DEST_DIR, 2018)
	adjust_module('.*BRAM_NUM', '.*MEM_NUM', tcpBRAM_rep, tcpMEM_rep, tcpIn_dir, IP_DEST_DIR, 2019)
	return
##############################################################

