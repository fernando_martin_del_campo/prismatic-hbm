source output_tcl/preamble.tcl
source output_tcl/update_ip.tcl
source output_tcl/clocking.tcl
source output_tcl/instances.tcl
source output_tcl/properties.tcl
source output_tcl/connections.tcl
source output_tcl/addresses.tcl
#source output_tcl/ila.tcl


#connect_bd_intf_net [get_bd_intf_ports ref_clk] [get_bd_intf_pins clk_wiz_0/CLK_IN1_D]

set_property SOURCE_SET sources_1 [get_filesets sim_1]
add_files -fileset sim_1 -norecurse -scan_for_includes ${sourcesDir}/prismatic_hbm_tb.v
import_files -fileset sim_1 -norecurse ${sourcesDir}/prismatic_hbm_tb.v
update_compile_order -fileset sim_1
set_property top prismatic_hbm_tb [get_filesets sim_1]

save_bd_design
#start_gui
#cd Prismatic_prj/
#update_compile_order -fileset sources_1
#update_ip_catalog -rebuild -scan_changes
#generate_target Simulation [get_files Compass.srcs/sources_1/bd/compass_design/compass_design.bd]
#launch_simulation
#open_wave_config {compass_tb_behav.wcfg}

#restart

