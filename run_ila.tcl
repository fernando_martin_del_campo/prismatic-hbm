open_hw
connect_hw_server -url localhost:3121
open_hw_target {localhost:3121/xilinx_tcf/Digilent/210308A65C84}

set_property PROGRAM.FILE {Prismatic_prj/Prismatic.runs/impl_1/prismatic_hbm_design_wrapper.bit} [get_hw_devices xcvu37p_0]
set_property PROBES.FILE {Prismatic_prj/Prismatic.runs/impl_1/prismatic_hbm_design_wrapper.ltx} [get_hw_devices xcvu37p_0]
set_property FULL_PROBES.FILE {Prismatic_prj/Prismatic.runs/impl_1/prismatic_hbm_design_wrapper.ltx} [get_hw_devices xcvu37p_0]

program_hw_devices [get_hw_devices xcvu37p_0]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]

set_property CONTROL.DATA_DEPTH 1 [get_hw_ilas -of_objects [get_hw_devices xcvu37p_0] -filter {CELL_NAME=~"prismatic_design_i/system_ila_0/inst/ila_lib"}]
set_property CONTROL.TRIGGER_POSITION 0 [get_hw_ilas -of_objects [get_hw_devices xcvu37p_0] -filter {CELL_NAME=~"prismatic_design_i/system_ila_0/inst/ila_lib"}]
set_property TRIGGER_COMPARE_VALUE eq1'b1 [get_hw_probes prismatic_design_i/system_ila_0/inst/probe0_1 -of_objects [get_hw_ilas -of_objects [get_hw_devices xcvu37p_0] -filter {CELL_NAME=~"prismatic_design_i/system_ila_0/inst/ila_lib"}]]

run_hw_ila [get_hw_ilas -of_objects [get_hw_devices xcvu37p_0] -filter {CELL_NAME=~"prismatic_design_i/system_ila_0/inst/ila_lib"}]

write_hw_ila_data -force my_hw_ila_data_file.zip [upload_hw_ila_data hw_ila_1]
