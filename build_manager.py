#!/usr/bin/python3

import os
import subprocess
import glob
import sys
import re
import shutil
import glob
import struct
import zipfile

from system_setup.generate_base_tcl import *

###-> check that Vivado variables are on in session
#
if not ("Vivado" in os.environ['PATH']):
	print("\n\n\tYou need to run xilinx_env first\n\n")
	sys.exit(1)
#
###


####-> remove files
#
ip_regex = re.compile(".*zip")                                  # find zip files
ip_dirs = os.listdir('sources/ip')                               # get list of files and directories on ip dir
filtered = [i for i in ip_dirs if not ip_regex.search(i)]       # remove zip files from list

for i in filtered:                                              # delete non-zip files and directories
	shutil.rmtree('sources/ip/' + i)

for f in glob.glob("vivado*"):                                   # remove old logs
	os.remove(f)

if os.path.exists("prismatic_hbm_x.tcl"):
	os.remove("prismatic_hbm_x.tcl")

shutil.rmtree('Prismatic_prj', True)                                  # remove previous Vivado project

#
###
ILA_ON = True

module_dir=os.getcwd()

input_list = open('input_list/list.csv')
next(input_list)

for row in input_list:
	req_gen_rep, parse_rep, hash_rep, channel_rep, read_rep, write_rep, read_reg_rep, write_reg_rep, tcpBRAM_rep, tcpMEM_rep, mem_chl_rep, requests, words, r_mems, w_mems, b_mems, chan_mode = row.split(',')

	###-> Generate all the tcl scripts (instances, update_ip, properties, addresses)
	#

	generate_base_tcl( req_gen_rep, parse_rep, hash_rep, channel_rep, read_rep, write_rep, read_reg_rep, write_reg_rep, tcpBRAM_rep, tcpMEM_rep, mem_chl_rep, requests, words, r_mems, w_mems, b_mems, chan_mode )
	#
	###


	###-> Add ILA to design
	#
	os.chdir(module_dir)
	out_file = open('prismatic_hbm_x.tcl','a')
	with open("prismatic_hbm.tcl") as file_ip:
		for row in file_ip:
			if ILA_ON:
				new_row=re.sub('#(source output_tcl/ila.tcl)', r'\1', row)
			else:
				new_row=re.sub('(^source output_tcl/ila.tcl)', r'#\1', row)
			out_file.write(new_row)
	out_file.close()
	#
	###


	###-> Create current version of Prismatic
	#
	subprocess.run(['vivado','-mode','batch','-source','prismatic_hbm_x.tcl'])
	#
	###
