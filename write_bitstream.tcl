cd /home/fmartin/projects/in_work/Prismatic_hbm-1.0/Prismatic_prj


open_project Prismatic.xpr
open_hw
connect_hw_server -url localhost:3121
current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210308A65C84]
set_property PARAM.FREQUENCY 10000000 [get_hw_targets */xilinx_tcf/Digilent/210308A65C84]
open_hw_target
set suffix [lindex $argv 0]
set base_dir "/home/fmartin/projects/in_work/Prismatic_hbm-1.0/Prismatic_prj/Prismatic.runs/impl_1/"
set base_file "prismatic_hbm_design_wrapper"
set bitstream_s ${base_dir}${base_file}.bit
set probes_s ${base_dir}${base_file}.ltx
set_property PROGRAM.FILE $bitstream_s [get_hw_devices xcvu37p_0]
set_property PROBES.FILE $probes_s [get_hw_devices xcvu37p_0]
set_property FULL_PROBES.FILE $probes_s [get_hw_devices xcvu37p_0]
current_hw_device [get_hw_devices xcvu37p_0]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]

program_hw_devices [get_hw_devices xcvu37p_0]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]
