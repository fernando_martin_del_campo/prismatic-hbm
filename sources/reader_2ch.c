#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#define MEM_BASE_A 0x80000000
#define MEM_BASE_B 0xA0000000

volatile unsigned int * gpio_b = (unsigned int *)0x40000000;
volatile unsigned int * gpio2_b = (unsigned int *)0x40000008;
volatile unsigned int * mem_A  = (unsigned int *)MEM_BASE_A;
volatile unsigned int * mem_B  = (unsigned int *)MEM_BASE_B;

#define xprint xil_printf
unsigned int index_mem;
int total;


int main()
{
    init_platform();
    char request_words = 3;

    xprint("Hello World\n\r");


    xprint("Clearing the memory ...\n\r");
    for(index_mem=0; index_mem<0x8000000; index_mem++)
    {
        *(mem_A+index_mem) = 0;
        *(mem_B+index_mem) = 0;
    }

    xprint("Done. Starting the system ...\n\r");
    *gpio2_b = 1;
    xprint("Done. System running ...\n\r");
    while(1)
    {
    	xprint("Req %u ...\n\r",*gpio_b);
        if ( *gpio_b == 41)
         break;
    }

    xprint("System done. Reading non-zero entries ...\n\r");

    xprint("\tChannel A ...\n\r");

    for(index_mem=0; index_mem<0x8000000; index_mem++)
    {
       if ( *(mem_A+index_mem) != 0)
       {
    	   if ( index_mem % (request_words + 1) == 0 )
    		   xprint("\n\r");

           xprint("\t\tMem[%X]: %08x\n\r", MEM_BASE_A + (index_mem)*4,*(mem_A+index_mem));

           ++total;
       }
    }
    xprint("\tChannel B ...\n\r");

    for(index_mem=0; index_mem<0x8000000; index_mem++)
    {
        if ( *(mem_B+index_mem) != 0)
        {
           if ( index_mem % (request_words + 1) == 0 )
        	   xprint("\n\r");

           xprint("\t\tMem[%08x]: %08x\n\r", MEM_BASE_B + (index_mem)*4,*(mem_B+index_mem));
           ++total;
        }
    }
    //xprint("Mem[L]: %08x\n\r", *ddr_last);
    xprint("Memory checked\n\r");
    xprint("request_words = %d\n\r",total/request_words);

    cleanup_platform();
    return 0;
}
