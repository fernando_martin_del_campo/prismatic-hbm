`timescale 1 ps / 1 ps

module prismatic_hbm_design_wrapper
   (refclk300_n,
    refclk300_p,
    sfp0_refclk_n,
    sfp0_refclk_p,
    sys_rst_n);
   input refclk300_n;
   input refclk300_p;
   input sfp0_refclk_n;
   input sfp0_refclk_p;
   input sys_rst_n;

  wire refclk300_n;
  wire refclk300_p;
  wire sfp0_refclk_n;
  wire sfp0_refclk_p;
  wire sys_rst_n;

 prismatic_hbm_design prismatic_design_i
       (.refclk300_n(refclk300_n),
        .refclk300_p(refclk300_p),
        .sfp0_refclk_n(sfp0_refclk_n),
        .sfp0_refclk_p(sfp0_refclk_p),
        .sys_rst_n(sys_rst_n));
endmodule