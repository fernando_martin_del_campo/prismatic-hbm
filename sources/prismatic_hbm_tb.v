`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/05/2018 03:11:29 PM
// Design Name: 
// Module Name: compass_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module prismatic_hbm_tb(
 
    );
    
    reg refclk300_n,  refclk300_p, sys_rst_n;
    
    prismatic_hbm_design_wrapper dut(.sys_rst_n(sys_rst_n),.refclk300_n(refclk300_n),.refclk300_p(refclk300_p));
    
    initial
    begin
       refclk300_n = 0;
       refclk300_p = 1;
       sys_rst_n = 0;
        
       //$monitor("(Req_gen)  : %h", dut.compass_design_i.req_gen_0.req_out);
       //$monitor("(Parser)   : %h,  %u", dut.compass_design_i.parse_0.req_write, dut.compass_design_i.enable_logic_0.Res);
       //$monitor("(Memory)   : %h   %u", dut.compass_design_i.memory_0.dina, dut.compass_design_i.memory_0.ena);
       //$monitor("\t(Hash)   : %h", dut.compass_design_i.hash_gen_0.data_out);
       //$monitor("\t(Channel): %h", dut.compass_design_i.channel_sel_0.data_out);
       //$monitor("\t\t(times_out)  : %d", dut.compass_design_i.req_gen_0.times_out);
       //$monitor("\t(HASH) Out: %h", dut.compass_design_i.channel_sel_0.data_out);
       $monitor("Memory A:   %h", dut.prismatic_design_i.memory_reader_A.data_dbg);
       $monitor("Memory B:   %h", dut.prismatic_design_i.memory_reader_B.data_dbg);
     
       #100
       sys_rst_n = 1;
    end
    always
    begin
       #5 refclk300_n <= ~refclk300_n;
       refclk300_p <= ~refclk300_p;
    end
       

endmodule
