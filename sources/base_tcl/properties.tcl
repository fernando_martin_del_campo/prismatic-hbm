current_bd_instance TCP_Subsystem

set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {not}] [get_bd_cells net_rst_in]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0}] [get_bd_cells pause_c]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0}] [get_bd_cells count_s_c]

set_property -dict [list CONFIG.NUM_PORTS {3}] [get_bd_cells xlconcat_0]

set_property -dict [list CONFIG.C_SIZE {3}] [get_bd_cells util_reduced_logic_0]

set_property -dict [list CONFIG.CONST_WIDTH {33} CONFIG.CONST_VAL {0xE0000000}] [get_bd_cells TCP_wrapper_c33]

set_property -dict [list CONFIG.CONST_VAL {0}] [get_bd_cells arp_en_c]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0x68}] [get_bd_cells big_req_c]
>> 9 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z}] [get_bd_cells big_mems_c]

set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {XYZ}] [get_bd_cells tmp_mems_c]

set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {not}\
                         CONFIG.LOGO_FILE {data/sym_notgate.png}\
] [get_bd_cells reset_inv]

set_property -dict [list\
               CONFIG.C_IP_B0 {120} CONFIG.C_IP_B1 {2} CONFIG.C_IP_B2 {1} CONFIG.C_IP_B3 {10}\
               CONFIG.C_GATEWAY_B0 {100} CONFIG.C_GATEWAY_B1 {2} CONFIG.C_GATEWAY_B2 {1} CONFIG.C_GATEWAY_B3 {10}\
               CONFIG.C_SUBNET_B1 {255} CONFIG.C_SUBNET_B2 {255} CONFIG.C_SUBNET_B3 {255}\
               CONFIG.C_MAC {0x42200A010278}\
] [get_bd_cells ip_constant_block_0]

set_property -dict [list CONFIG.INTERFACE_TYPE {AXI_STREAM}\
                         CONFIG.Reset_Type {Asynchronous_Reset}\
                         CONFIG.TDATA_NUM_BYTES {8}\
                         CONFIG.Enable_TLAST {true}\
                         CONFIG.TSTRB_WIDTH {8}\
                         CONFIG.TKEEP_WIDTH {8}\
                         CONFIG.FIFO_Implementation_wach {Common_Clock_Distributed_RAM}\
                         CONFIG.Full_Threshold_Assert_Value_wach {15}\
                         CONFIG.Empty_Threshold_Assert_Value_wach {14}\
                         CONFIG.FIFO_Implementation_wdch {Common_Clock_Builtin_FIFO}\
                         CONFIG.FIFO_Implementation_wrch {Common_Clock_Distributed_RAM}\
                         CONFIG.Full_Threshold_Assert_Value_wrch {15}\
                         CONFIG.Empty_Threshold_Assert_Value_wrch {14}\
                         CONFIG.FIFO_Implementation_rach {Common_Clock_Distributed_RAM}\
                         CONFIG.Full_Threshold_Assert_Value_rach {15}\
                         CONFIG.Empty_Threshold_Assert_Value_rach {14}\
                         CONFIG.FIFO_Implementation_rdch {Common_Clock_Builtin_FIFO}\
                         CONFIG.FIFO_Implementation_axis {Common_Clock_Block_RAM}\
                         CONFIG.FIFO_Application_Type_axis {Packet_FIFO}\
                         CONFIG.Input_Depth_axis {2048}\
                         CONFIG.Full_Threshold_Assert_Value_axis {2047}\
                         CONFIG.Empty_Threshold_Assert_Value_axis {2046}\
] [get_bd_cells fifo_generator_0]


current_bd_instance network_module
set_property -dict [list CONFIG.CONST_VAL {0}] [get_bd_cells gnd]

set_property -dict [list CONFIG.CONST_WIDTH {58} CONFIG.CONST_VAL {0}] [get_bd_cells seed_c]

set_property -dict [list CONFIG.CONST_WIDTH {15} CONFIG.CONST_VAL {1522}] [get_bd_cells max_pack_l]

set_property -dict [list CONFIG.CONST_WIDTH {8} CONFIG.CONST_VAL {64}] [get_bd_cells min_pack_l]

set_property -dict [list CONFIG.CONST_WIDTH {3} CONFIG.CONST_VAL {5}] [get_bd_cells transc_clk_sel]

set_property -dict [list CONFIG.CONST_WIDTH {3} CONFIG.CONST_VAL {0}] [get_bd_cells loop_c]

set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells user_reset_and]

set_property -dict [list CONFIG.LINE_RATE {10} CONFIG.GT_REF_CLK_FREQ {161.1328125} CONFIG.GT_GROUP_SELECT {Quad_X1Y6} CONFIG.LANE1_GT_LOC {X1Y24}] [get_bd_cells xxv_ethernet_0]

set_property -dict [list CONFIG.HAS_TKEEP.VALUE_SRC USER\
                         CONFIG.HAS_TLAST.VALUE_SRC USER\
                         CONFIG.TUSER_WIDTH.VALUE_SRC USER\
                         CONFIG.TDEST_WIDTH.VALUE_SRC USER\
                         CONFIG.TID_WIDTH.VALUE_SRC USER\
                         CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER\
] [get_bd_cells axis_register_slice_in]

set_property -dict [list CONFIG.TDATA_NUM_BYTES {8} CONFIG.TUSER_WIDTH {4} CONFIG.HAS_TKEEP {1} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_in]

set_property -dict [list CONFIG.CONST_WIDTH {56} CONFIG.CONST_VAL {0}] [get_bd_cells tx_pre_c]

set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {not} CONFIG.LOGO_FILE {data/sym_notgate.png}] [get_bd_cells reset_inv]

set_property -dict [list CONFIG.INTERFACE_TYPE {AXI_STREAM}\
                         CONFIG.Reset_Type {Asynchronous_Reset}\
                         CONFIG.TDATA_NUM_BYTES {8}\
                         CONFIG.Enable_TLAST {true}\
                         CONFIG.TSTRB_WIDTH {8}\
                         CONFIG.TKEEP_WIDTH {8}\
                         CONFIG.FIFO_Implementation_wach {Common_Clock_Distributed_RAM}\
                         CONFIG.Full_Threshold_Assert_Value_wach {15}\
                         CONFIG.Empty_Threshold_Assert_Value_wach {14}\
                         CONFIG.FIFO_Implementation_wdch {Common_Clock_Builtin_FIFO}\
                         CONFIG.FIFO_Implementation_wrch {Common_Clock_Distributed_RAM}\
                         CONFIG.Full_Threshold_Assert_Value_wrch {15}\
                         CONFIG.Empty_Threshold_Assert_Value_wrch {14}\
                         CONFIG.FIFO_Implementation_rach {Common_Clock_Distributed_RAM}\
                         CONFIG.Full_Threshold_Assert_Value_rach {15}\
                         CONFIG.Empty_Threshold_Assert_Value_rach {14}\
                         CONFIG.FIFO_Implementation_rdch {Common_Clock_Builtin_FIFO}\
                         CONFIG.FIFO_Implementation_axis {Common_Clock_Block_RAM}\
                         CONFIG.FIFO_Application_Type_axis {Packet_FIFO} CONFIG.Input_Depth_axis {2048}\
                         CONFIG.Full_Threshold_Assert_Value_axis {2047}\
                         CONFIG.Empty_Threshold_Assert_Value_axis {2046}\
] [get_bd_cells fifo_generator_0]

current_bd_instance

set inter_axi_read_group [list\
>> rep 10 $xcon_bram_read_X\
]

set inter_axi_write_group [list\
>> rep 10 $xcon_bram_write_X\
]

set_property -dict [ list \
 CONFIG.NUM_SI {2} \
>> 6 CONFIG.NUM_MI {Z}
] $inter_axi_read_group

set_property -dict [ list \
 CONFIG.NUM_SI {2} \
>> 7 CONFIG.NUM_MI {Z}
] $inter_axi_write_group

#>> 10 set_property -dict [list CONFIG.NUM_MI {Z} CONFIG.NUM_SI {4} CONFIG.NUM_CLKS {2}] [get_bd_cells xcon_memory_A]
set_property -dict [list CONFIG.NUM_MI {14} CONFIG.NUM_SI {2}] [get_bd_cells xcon_memory_A]
set_property -dict [list CONFIG.STRATEGY {2} CONFIG.NUM_SI {2} CONFIG.NUM_MI {1} CONFIG.M00_HAS_REGSLICE {4}\
                         CONFIG.S00_HAS_REGSLICE {4} CONFIG.S01_HAS_REGSLICE {4}] [get_bd_cells xcon_memory_0]
set_property -dict [list CONFIG.STRATEGY {2} CONFIG.S00_HAS_DATA_FIFO {2} CONFIG.S01_HAS_DATA_FIFO {2}]\
                         [get_bd_cells xcon_memory_0]
set_property -dict [list CONFIG.STRATEGY {2} CONFIG.NUM_SI {2} CONFIG.NUM_MI {1} CONFIG.M00_HAS_REGSLICE {4}\
                         CONFIG.S00_HAS_REGSLICE {4} CONFIG.S01_HAS_REGSLICE {4}] [get_bd_cells xcon_memory_1]
set_property -dict [list CONFIG.STRATEGY {2} CONFIG.S00_HAS_DATA_FIFO {2} CONFIG.S01_HAS_DATA_FIFO {2}]\
                         [get_bd_cells xcon_memory_1]

set_property -dict [list CONFIG.NUM_SI {2} CONFIG.NUM_MI {1} CONFIG.M00_HAS_REGSLICE {4} CONFIG.S00_HAS_REGSLICE {4} CONFIG.S01_HAS_REGSLICE {4}] [get_bd_cells xcon_memory_1]
set_property -dict [list CONFIG.STRATEGY {2} CONFIG.S00_HAS_DATA_FIFO {2} CONFIG.S01_HAS_DATA_FIFO {2}] [get_bd_cells xcon_memory_1]

>> 10 set_property -dict [list CONFIG.NUM_SI {Z} CONFIG.NUM_MI {1}] [get_bd_cells xcon_write_mem_A]
>> 10 set_property -dict [list CONFIG.NUM_SI {Z} CONFIG.NUM_MI {1}] [get_bd_cells xcon_big_mem_A]
#>> 10 set_property -dict [list CONFIG.NUM_SI {Z}] [get_bd_cells xcon_read_mem_A]
>> 10 set_property -dict [list CONFIG.NUM_MI {1} CONFIG.NUM_SI {Z}] [get_bd_cells xcon_read_mem_A]

set_property -dict [ list CONFIG.C_OPERATION {not} CONFIG.C_SIZE {1} ] $logic_reset_op

>> rep2 10 1 set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {or} CONFIG.LOGO_FILE {data/sym_orgate.png}] $ready_in_w_or_X_Y

>> rep 1 set_property -dict [list CONFIG.C_SIZE {6} ] $channel_mask_and_X

>> rep2 1 10 set_property -dict [list CONFIG.CONST_WIDTH {6} CONFIG.CONST_VAL {#} ] $channel_mask_X

>> rep2 1 10 set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {or} CONFIG.LOGO_FILE {data/sym_orgate.png}] $read_req_wrtr_or_X_Y

#>> rep2 1 2 set_property -dict [ list CONFIG.C_SIZE {32} ] $hash_mask_and_X_Y

set_property -dict [list CONFIG.SINGLE_PORT_BRAM {0}] $inter_axi_read_group

set_property -dict [list CONFIG.SINGLE_PORT_BRAM {0}] $inter_axi_write_group

>> rep2 1 10 set_property -dict [list CONFIG.CONST_WIDTH {6} CONFIG.CONST_VAL {_Y_}] [get_bd_cells enable_const_X_Y]

>> rep 10 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {16}] [get_bd_cells size_limit_X]

#>> rep2 1 2 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0x07FFFFFF}] [get_bd_cells hash_mask_X_Y ]

>> rep2 1 2 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {48}] [get_bd_cells seed_c_X_Y]
>> rep2 1 2 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {48}] [get_bd_cells seed_ch_c_X_Y]

>> rep 10 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {8}] [get_bd_cells register_offset_X]

>> rep 10 set_property -dict [list CONFIG.CONST_WIDTH {33} CONFIG.CONST_VAL {MEMORY_BASE}] [get_bd_cells mem_base_address_X]

>> rep 1 set_property -dict [list CONFIG.CONST_WIDTH {18} CONFIG.CONST_VAL {cnst_val_2}] [get_bd_cells num_channels_c_X]

set bram_grp [list\
>> rep2 10 6 $bram_read_X_Y\
>> rep2 10 7 $bram_write_X_Y\
]

set_property -dict [list\
 CONFIG.Memory_Type {True_Dual_Port_RAM}\
 CONFIG.Assume_Synchronous_Clk {true}\
 CONFIG.Enable_B {Use_ENB_Pin}\
 CONFIG.Use_RSTB_Pin {true}\
 CONFIG.Port_B_Clock {100}\
 CONFIG.Port_B_Write_Rate {50}\
 CONFIG.Port_B_Enable_Rate {100}\
] $bram_grp

>> rep2 1 10 set_property -dict [list CONFIG.C_OPERATION {not} CONFIG.LOGO_FILE {data/sym_notgate.png} CONFIG.C_SIZE {1}] [get_bd_cells parser_rw_not_X_Y]

>> rep2 1 10 set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells reg_arb_R_and_X_Y]
>> rep2 1 10 set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells reg_arb_W_and_X_Y]
>> rep2 1 10 set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells reg_arb_BW_and_X_Y]

set_property -dict [ list \
CONFIG.HBM_MMCM_FBOUT_MULT0 {112} \
CONFIG.USER_AXI_CLK_FREQ {400} \
CONFIG.USER_CLK_SEL_LIST0 {AXI_00_ACLK} \
CONFIG.USER_CLK_SEL_LIST1 {AXI_16_ACLK} \
CONFIG.USER_HBM_CP_0 {5} \
CONFIG.USER_HBM_CP_1 {5} \
CONFIG.USER_HBM_DENSITY {8GB} \
CONFIG.USER_HBM_FBDIV_0 {32} \
CONFIG.USER_HBM_FBDIV_1 {32} \
CONFIG.USER_HBM_HEX_CP_RES_0 {0x0000A500} \
CONFIG.USER_HBM_HEX_CP_RES_1 {0x0000A500} \
CONFIG.USER_HBM_HEX_FBDIV_CLKOUTDIV_0 {0x00000802} \
CONFIG.USER_HBM_HEX_FBDIV_CLKOUTDIV_1 {0x00000802} \
CONFIG.USER_HBM_STACK {2} \
CONFIG.USER_HBM_TCK_0 {800} \
CONFIG.USER_HBM_TCK_1 {800} \
CONFIG.USER_MC0_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC0_EN_DATA_MASK {true} \
CONFIG.USER_MC10_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC10_EN_DATA_MASK {true} \
CONFIG.USER_MC11_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC11_EN_DATA_MASK {true} \
CONFIG.USER_MC12_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC12_EN_DATA_MASK {true} \
CONFIG.USER_MC13_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC13_EN_DATA_MASK {true} \
CONFIG.USER_MC14_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC14_EN_DATA_MASK {true} \
CONFIG.USER_MC15_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC15_EN_DATA_MASK {true} \
CONFIG.USER_MC1_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC1_EN_DATA_MASK {true} \
CONFIG.USER_MC2_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC2_EN_DATA_MASK {true} \
CONFIG.USER_MC3_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC3_EN_DATA_MASK {true} \
CONFIG.USER_MC4_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC4_EN_DATA_MASK {true} \
CONFIG.USER_MC5_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC5_EN_DATA_MASK {true} \
CONFIG.USER_MC6_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC6_EN_DATA_MASK {true} \
CONFIG.USER_MC7_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC7_EN_DATA_MASK {true} \
CONFIG.USER_MC8_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC8_EN_DATA_MASK {true} \
CONFIG.USER_MC9_ENABLE_ECC_CORRECTION {false} \
CONFIG.USER_MC9_EN_DATA_MASK {true} \
CONFIG.USER_MC_ENABLE_08 {TRUE} \
CONFIG.USER_MC_ENABLE_09 {TRUE} \
CONFIG.USER_MC_ENABLE_10 {TRUE} \
CONFIG.USER_MC_ENABLE_11 {TRUE} \
CONFIG.USER_MC_ENABLE_12 {TRUE} \
CONFIG.USER_MC_ENABLE_13 {TRUE} \
CONFIG.USER_MC_ENABLE_14 {TRUE} \
CONFIG.USER_MC_ENABLE_15 {TRUE} \
CONFIG.USER_MC_ENABLE_APB_01 {TRUE} \
CONFIG.USER_PHY_ENABLE_08 {TRUE} \
CONFIG.USER_PHY_ENABLE_09 {TRUE} \
CONFIG.USER_PHY_ENABLE_10 {TRUE} \
CONFIG.USER_PHY_ENABLE_11 {TRUE} \
CONFIG.USER_PHY_ENABLE_12 {TRUE} \
CONFIG.USER_PHY_ENABLE_13 {TRUE} \
CONFIG.USER_PHY_ENABLE_14 {TRUE} \
CONFIG.USER_PHY_ENABLE_15 {TRUE} \
CONFIG.USER_SAXI_01 {true} \
CONFIG.USER_SAXI_02 {true} \
CONFIG.USER_SAXI_03 {true} \
CONFIG.USER_SAXI_04 {true} \
CONFIG.USER_SAXI_05 {true} \
CONFIG.USER_SAXI_06 {true} \
CONFIG.USER_SAXI_07 {true} \
CONFIG.USER_SAXI_08 {true} \
CONFIG.USER_SAXI_09 {true} \
CONFIG.USER_SAXI_10 {true} \
CONFIG.USER_SAXI_11 {true} \
CONFIG.USER_SAXI_12 {true} \
CONFIG.USER_SAXI_13 {true} \
CONFIG.USER_SAXI_16 {false} \
CONFIG.USER_SAXI_17 {false} \
CONFIG.USER_SAXI_18 {false} \
CONFIG.USER_SAXI_19 {false} \
CONFIG.USER_SAXI_20 {false} \
CONFIG.USER_SAXI_21 {false} \
CONFIG.USER_SAXI_22 {false} \
CONFIG.USER_SAXI_23 {false} \
CONFIG.USER_SAXI_24 {false} \
CONFIG.USER_SAXI_25 {false} \
CONFIG.USER_SAXI_26 {false} \
CONFIG.USER_SAXI_27 {false} \
CONFIG.USER_SAXI_28 {false} \
CONFIG.USER_SAXI_29 {false} \
CONFIG.USER_SAXI_30 {false} \
CONFIG.USER_SAXI_31 {false} \
CONFIG.USER_SWITCH_ENABLE_01 {TRUE} \
] $hbm_inst


set_property -dict [list CONFIG.CONST_VAL {0} ] [get_bd_cells zero1_inst]
set_property -dict [list CONFIG.CONST_VAL {0} CONFIG.CONST_WIDTH {22}] [get_bd_cells zero22_inst]
set_property -dict [list CONFIG.CONST_VAL {0} CONFIG.CONST_WIDTH {32}] [get_bd_cells zero32_inst]

set_property -dict [list CONFIG.C_BUF_TYPE {BUFG}] [get_bd_cells refclk_bufg]

set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0}] [get_bd_cells count_start_c]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {cnst_val}] [get_bd_cells counter_xor_c]
set_property -dict [list CONFIG.C_SIZE {32} CONFIG.C_OPERATION {xor} CONFIG.LOGO_FILE {data/sym_xorgate.png}] [get_bd_cells counter_xor]
set_property -dict [list CONFIG.C_SIZE {32} CONFIG.C_OPERATION {not} CONFIG.LOGO_FILE {data/sym_notgate.png}] [get_bd_cells counter_inv]
set_property -dict [list CONFIG.C_SIZE {32}] [get_bd_cells xor_reduce]

set_property -dict [list CONFIG.NUM_SI {1} CONFIG.NUM_MI {1}] [get_bd_cells xcon_tcp_mem]
set_property -dict [list CONFIG.C_M_AXI_DATA_OUT_V_TARGET_ADDR {0xC0000000}] [get_bd_cells TCP_Subsystem/tcp_in_0]
set_property -dict [list CONFIG.C_M_AXI_READ_REQ_V_TARGET_ADDR {0xC0002000}] [get_bd_cells TCP_Subsystem/tcp_in_0]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0xC0000000}] [get_bd_cells req_h_mem_b]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0xF0000000}] [get_bd_cells req_h_mem_m]
set_property -dict [list CONFIG.C_M_AXI_DATA_IN_V_TARGET_ADDR {0xC0002000} CONFIG.C_M_AXI_DATA_IN_MEM_V_TARGET_ADDR {0x00000000}] [get_bd_cells TCP_Subsystem/tcp_out_0]
set_property -dict [list CONFIG.C_M_AXI_DATA_OUT_BIG_V_TARGET_ADDR {0xF0000000}] [get_bd_cells TCP_Subsystem/tcp_in_0]

>> 0 set_property -dict [list CONFIG.NUM_SI {Z/2+} ] [get_bd_cells xcon_tcp_bram_A]
>> 8 set_property -dict [list CONFIG.NUM_MI {Z} ] [get_bd_cells xcon_tcp_bram_A]
>> 0 set_property -dict [list CONFIG.NUM_SI {Z/2+} CONFIG.NUM_MI {1}] [get_bd_cells xcon_mem_temp_A]

>> 10 set_property -dict [list CONFIG.NUM_SI {Z} CONFIG.NUM_MI {1}] [get_bd_cells xcon_in_write_A]
>> 10 set_property -dict [list CONFIG.NUM_SI {Z} CONFIG.NUM_MI {1}] [get_bd_cells xcon_in_read_A]
>> 10 set_property -dict [list CONFIG.NUM_SI {Z} CONFIG.NUM_MI {1}] [get_bd_cells xcon_in_big_A]
set_property -dict [list CONFIG.NUM_SI {4} CONFIG.NUM_MI {XYZ+}] [get_bd_cells xcon_in_req_A]

>> rep 8 set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_tcp_ctrl_X]
>> repMul2 set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_req_ctrl_X]

>> repBRAMw 10 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {BASE_BRAM}] [get_bd_cells req_base_w_c_X]
>> repBRAMb 10 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {BASE_BRAM}] [get_bd_cells req_base_b_c_X]
>> repBRAMr 10 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {BASE_BRAM}] [get_bd_cells req_base_r_c_X]

>> rep_hl 0 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z}] [get_bd_cells handler_num_b_X]
>> rep_hl 0 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z}] [get_bd_cells handler_num_m_X]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {8192}] [get_bd_cells mem_size_b]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {2097152}] [get_bd_cells mem_size_m]
>> 0 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z/2}] [get_bd_cells num_mem_c]

set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells mem_calib_and]

>> rep2 10 5 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z+}] [get_bd_cells own_big_w_c_X_Y]
>> rep2 10 5 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z+}] [get_bd_cells own_mem_r_c_X_Y]
>> rep2 10 5 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {Z+}] [get_bd_cells own_mem_w_c_X_Y]

set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {8192}] [get_bd_cells BRAM_size_c]
set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {0}] [get_bd_cells reducer_mode_c]

>> 11 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {ZM}] [get_bd_cells tmp_mems_b_c]
>> 12 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {ZM}] [get_bd_cells tmp_mems_r_c]
>> 13 set_property -dict [list CONFIG.CONST_WIDTH {32} CONFIG.CONST_VAL {ZM}] [get_bd_cells tmp_mems_w_c]

set_property -dict [list CONFIG.STRATEGY {2} CONFIG.S00_HAS_DATA_FIFO {2} CONFIG.S01_HAS_DATA_FIFO {2}\
                CONFIG.S02_HAS_DATA_FIFO {2} CONFIG.S03_HAS_DATA_FIFO {2}] [get_bd_cells xcon_memory_A]

set_property -dict [list CONFIG.S00_HAS_REGSLICE {4} CONFIG.S01_HAS_REGSLICE {4} CONFIG.S02_HAS_REGSLICE {4}\
                         CONFIG.S03_HAS_REGSLICE {4}] [get_bd_cells xcon_memory_A]                                                                
set_property -dict [list CONFIG.M00_HAS_REGSLICE {4} CONFIG.M01_HAS_REGSLICE {4} CONFIG.M02_HAS_REGSLICE {4}\
                         CONFIG.M03_HAS_REGSLICE {4} CONFIG.M04_HAS_REGSLICE {4} CONFIG.M05_HAS_REGSLICE {4}\
                         CONFIG.M06_HAS_REGSLICE {4} CONFIG.M07_HAS_REGSLICE {4} CONFIG.M08_HAS_REGSLICE {4}\
                         CONFIG.M09_HAS_REGSLICE {4} CONFIG.M10_HAS_REGSLICE {4} CONFIG.M11_HAS_REGSLICE {4}\
                         CONFIG.M12_HAS_REGSLICE {4} CONFIG.M13_HAS_REGSLICE {4}] [get_bd_cells xcon_memory_A]     

set_property -dict [list CONFIG.S00_HAS_REGSLICE {4}] [get_bd_cells xcon_write_mem_A]
set_property -dict [list CONFIG.S00_HAS_REGSLICE {4}] [get_bd_cells xcon_big_mem_A]
set_property -dict [list CONFIG.S00_HAS_REGSLICE {4}] [get_bd_cells xcon_read_mem_A] 

>> rep2 1 2 set_property -dict [list CONFIG.NUM_OF_RAMS {Z}] [get_bd_cells hash_rd_X_Y]

set_property -dict [list\
>> rep_less 10 CONFIG.S0Z_HAS_REGSLICE {4}\
CONFIG.M00_HAS_REGSLICE {4}\
] [get_bd_cells xcon_in_big_A]

set_property -dict [list\
CONFIG.STRATEGY {2}\
>> rep_less 10 CONFIG.S0Z_HAS_DATA_FIFO {2}\
>> rep_less 10 CONFIG.S0Z_HAS_REGSLICE {4}\
CONFIG.M00_HAS_REGSLICE {4}\
] [get_bd_cells xcon_in_read_A]

set_property -dict [list\
CONFIG.STRATEGY {2}\
>> rep_less 10 CONFIG.S0Z_HAS_DATA_FIFO {2}\
>> rep_less 10 CONFIG.S0Z_HAS_REGSLICE {4}\
CONFIG.M00_HAS_REGSLICE {4}\
] [get_bd_cells xcon_in_write_A]

#new
set_property -dict [list\
CONFIG.STRATEGY {2}\
CONFIG.S00_HAS_DATA_FIFO {2}\
CONFIG.S01_HAS_DATA_FIFO {2}\
CONFIG.S02_HAS_DATA_FIFO {2}\
CONFIG.S03_HAS_DATA_FIFO {2}\
CONFIG.S00_HAS_REGSLICE {4}\
CONFIG.S01_HAS_REGSLICE {4}\
CONFIG.S02_HAS_REGSLICE {4}\
CONFIG.S03_HAS_REGSLICE {4}\
>> rep_plus CONFIG.M0Z_HAS_REGSLICE {4}\
] [get_bd_cells xcon_in_req_A]

set_property -dict [list\
CONFIG.STRATEGY {2}\
>> rep_less 10 CONFIG.S0Z_HAS_DATA_FIFO {2}\
>> rep_less 10 CONFIG.S0Z_HAS_REGSLICE {4}\
>> rep_less 8 CONFIG.M0Z_HAS_REGSLICE {4}\
] [get_bd_cells xcon_mem_temp_A]


set_property -dict [list\
CONFIG.STRATEGY {2}\
>> rep_less_h 0 CONFIG.S00_HAS_DATA_FIFO {2}\
>> rep_less_h 0 CONFIG.S0Z_HAS_REGSLICE {4}\
>> rep_less 8 CONFIG.M0Z_HAS_REGSLICE {4}\
] [get_bd_cells xcon_tcp_bram_A]

set_property -dict [list CONFIG.S00_HAS_REGSLICE {4}] [get_bd_cells xcon_tcp_mem]

set_property -dict [list\
CONFIG.STRATEGY {2}\
CONFIG.S00_HAS_REGSLICE {4}\
CONFIG.S01_HAS_REGSLICE {4}\
>> rep_less 6 CONFIG.M0Z_HAS_REGSLICE {4}\
] $inter_axi_read_group

set_property -dict [list\
CONFIG.STRATEGY {2}\
CONFIG.S00_HAS_REGSLICE {4}\
CONFIG.S01_HAS_REGSLICE {4}\
>> rep_less 7 CONFIG.M0Z_HAS_REGSLICE {4}\
] $inter_axi_write_group

#new
set_property -dict [list\
CONFIG.STRATEGY {2}\
CONFIG.M00_HAS_REGSLICE {4}\
>> rep_less 10 CONFIG.S0Z_HAS_REGSLICE {4}\
] [get_bd_cells xcon_read_mem_A]

#new
set_property -dict [list\
CONFIG.STRATEGY {2}\
CONFIG.M00_HAS_REGSLICE {4}\
>> rep_less 10 CONFIG.S0Z_HAS_REGSLICE {4}\
] [get_bd_cells xcon_write_mem_A]
