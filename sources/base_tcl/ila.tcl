set_property HDL_ATTRIBUTE.DEBUG true [get_bd_nets {xor_reduce_Res }]

connect_bd_net -net count_out [get_bd_pins cycle_counter_0/count_out]

set_property HDL_ATTRIBUTE.DEBUG true [get_bd_nets {count_out }]

apply_bd_automation -rule xilinx.com:bd_rule:debug -dict [list \
   [get_bd_nets xor_reduce_Res] {PROBE_TYPE "Trigger" CLK_SRC "None (Connect manually)" SYSTEM_ILA "Auto" } \
   [get_bd_nets count_out] {PROBE_TYPE "Data" CLK_SRC "/refclk_bufg/BUFG_O" SYSTEM_ILA "Auto" } \
   ]
