############################### CONNECT NODULES ###############################
#                                                                             #
#                                                                             #

### TCP stuff
#

current_bd_instance TCP_Subsystem

#connect_bd_intf_net [get_bd_intf_pins m_axi_Read_req] [get_bd_intf_pins tcp_in_0/m_axi_Read_req_V]

#connect_bd_intf_net [get_bd_intf_pins m_axi_req_done] [get_bd_intf_pins tcp_in_0/m_axi_req_done_V]
connect_bd_intf_net [get_bd_intf_pins gt_rx] -boundary_type upper [get_bd_intf_pins network_module/gt_rx]
connect_bd_intf_net [get_bd_intf_pins gt_tx] -boundary_type upper [get_bd_intf_pins network_module/gt_tx]

connect_bd_net [get_bd_pins aresetn] [get_bd_pins bit_synchronizer_1/async_in]

connect_bd_net [get_bd_pins clk_100] [get_bd_pins network_module/dlck_100MHz]

connect_bd_net [get_bd_pins locked] [get_bd_pins psreset_data/dcm_locked]

connect_bd_net [get_bd_pins sfp_clk_n] [get_bd_pins network_module/refclk_n]
connect_bd_net [get_bd_pins sft_clk_p] [get_bd_pins network_module/refclk_p]

connect_bd_net [get_bd_pins aresetn] [get_bd_pins reset_inv/Op1]

connect_bd_net [get_bd_pins network_reset] [get_bd_pins psreset_data/peripheral_aresetn]

connect_bd_net [get_bd_pins small_mems] [get_bd_pins tcp_in_0/small_mems]
connect_bd_net [get_bd_pins net_rst_in/Res] [get_bd_pins cycle_counter_128_0/reset]
connect_bd_net [get_bd_pins net_rst_in/Op1] [get_bd_pins psreset_data/peripheral_aresetn]
connect_bd_net [get_bd_pins cycle_counter_128_0/clk] [get_bd_pins network_module/clk_161]
connect_bd_net [get_bd_pins cycle_counter_128_0/start] [get_bd_pins tcp_in_0/start]
connect_bd_net [get_bd_pins pause_c/dout] [get_bd_pins cycle_counter_128_0/pause]
connect_bd_net [get_bd_pins count_s_c/dout] [get_bd_pins cycle_counter_128_0/count_start]

###
connect_bd_net [get_bd_pins ip_constant_block_0/ip] [get_bd_pins tcp_ip_wrapper_0/ipAddressIn]
connect_bd_net [get_bd_pins ip_constant_block_0/gateway] [get_bd_pins tcp_ip_wrapper_0/gatewayIn]
connect_bd_net [get_bd_pins ip_constant_block_0/subnet] [get_bd_pins tcp_ip_wrapper_0/subnetIn]
connect_bd_net [get_bd_pins ip_constant_block_0/mac] [get_bd_pins tcp_ip_wrapper_0/macAddressIn]

connect_bd_net [get_bd_pins hbm_calib] [get_bd_pins bit_synchronizer_0/async_in]

connect_bd_net [get_bd_pins psreset_data/peripheral_aresetn] [get_bd_pins network_module/axis_aresetn]
connect_bd_net [get_bd_pins tcp_ip_wrapper_0/aresetn] [get_bd_pins psreset_data/peripheral_aresetn]
connect_bd_net [get_bd_pins tcp_ip_wrapper_0/s_axictl_aresetn] [get_bd_pins psreset_data/peripheral_aresetn]


connect_bd_net [get_bd_pins fifo_generator_0/s_aresetn] [get_bd_pins psreset_data/peripheral_aresetn]

connect_bd_net [get_bd_pins network_module/clk_161] [get_bd_pins fifo_generator_0/s_aclk]

connect_bd_net [get_bd_pins tcp_ip_wrapper_0/s_axictl_aclk] [get_bd_pins network_module/clk_161]
connect_bd_net [get_bd_pins tcp_ip_wrapper_0/aclk] [get_bd_pins network_module/clk_161]

connect_bd_net [get_bd_pins psreset_data/slowest_sync_clk] [get_bd_pins network_module/clk_161]
connect_bd_net [get_bd_pins util_reduced_logic_0/Res] [get_bd_pins psreset_data/ext_reset_in]
connect_bd_net [get_bd_pins util_reduced_logic_0/Op1] [get_bd_pins xlconcat_0/dout]

connect_bd_net [get_bd_pins network_module/net_init_done] [get_bd_pins xlconcat_0/In0]
connect_bd_net [get_bd_pins bit_synchronizer_0/sync_out] [get_bd_pins xlconcat_0/In1]
connect_bd_net [get_bd_pins bit_synchronizer_1/sync_out] [get_bd_pins xlconcat_0/In2]

connect_bd_net [get_bd_pins bit_synchronizer_1/sync_clk] [get_bd_pins network_module/clk_161]
connect_bd_net [get_bd_pins bit_synchronizer_0/sync_clk] [get_bd_pins network_module/clk_161]

connect_bd_net [get_bd_pins TCP_wrapper_c/dout] [get_bd_pins tcp_ip_wrapper_0/configInEn]
connect_bd_net [get_bd_pins TCP_wrapper_c33/dout] [get_bd_pins tcp_ip_wrapper_0/m00_axi_offset]
connect_bd_net [get_bd_pins arp_en_c/dout] [get_bd_pins tcp_ip_wrapper_0/grat_arp_en]
connect_bd_net [get_bd_pins big_req_c/dout] [get_bd_pins tcp_in_0/Big_req]
connect_bd_net [get_bd_pins big_req_c/dout] [get_bd_pins tcp_out_0/Big_req]
connect_bd_net [get_bd_pins big_mems_c/dout] [get_bd_pins tcp_in_0/big_mems]

connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/AXI_M_Stream] -boundary_type upper\
                    [get_bd_intf_pins network_module/s_axis_tx]

connect_bd_intf_net -boundary_type upper [get_bd_intf_pins network_module/m_axis_rx]\
                                         [get_bd_intf_pins fifo_generator_0/S_AXIS]

connect_bd_intf_net [get_bd_intf_pins fifo_generator_0/M_AXIS] [get_bd_intf_pins tcp_ip_wrapper_0/AXI_S_Stream]

connect_bd_net [get_bd_pins reset_inv/Res] [get_bd_pins network_module/network_reset]
connect_bd_net [get_bd_pins tcp_in_0/ap_clk] [get_bd_pins network_module/clk_161]
connect_bd_net [get_bd_pins tcp_out_0/ap_clk] [get_bd_pins network_module/clk_161]
connect_bd_net [get_bd_pins tcp_out_0/ap_rst_n] [get_bd_pins psreset_data/peripheral_aresetn]
connect_bd_net [get_bd_pins tcp_in_0/ap_rst_n] [get_bd_pins psreset_data/peripheral_aresetn]

connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/m_axis_listen_port_status] [get_bd_intf_pins tcp_in_0/s_axis_listen_port_status_V]
connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/m_axis_notifications] [get_bd_intf_pins tcp_in_0/s_axis_notifications_V]
connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/m_axis_open_status] [get_bd_intf_pins tcp_in_0/s_axis_open_status_V]
connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/m_axis_rx_data] [get_bd_intf_pins tcp_in_0/s_axis_rx_data]
connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/m_axis_rx_metadata] [get_bd_intf_pins tcp_in_0/s_axis_rx_metadata_V_V]
connect_bd_intf_net [get_bd_intf_pins tcp_ip_wrapper_0/m_axis_tx_status] [get_bd_intf_pins tcp_out_0/s_axis_tx_status_V]

connect_bd_intf_net [get_bd_intf_pins tcp_in_0/m_axis_listen_port_V_V] [get_bd_intf_pins tcp_ip_wrapper_0/s_axis_listen_port]
connect_bd_intf_net [get_bd_intf_pins tcp_in_0/m_axis_read_package_V] [get_bd_intf_pins tcp_ip_wrapper_0/s_axis_read_package]
connect_bd_intf_net [get_bd_intf_pins tcp_in_0/m_axis_open_connection_V] [get_bd_intf_pins tcp_ip_wrapper_0/s_axis_open_connection]
connect_bd_intf_net [get_bd_intf_pins tcp_in_0/m_axis_close_connection_V_V] [get_bd_intf_pins tcp_ip_wrapper_0/s_axis_close_connection]

connect_bd_intf_net [get_bd_intf_pins tcp_out_0/m_axis_tx_metadata_V] [get_bd_intf_pins tcp_ip_wrapper_0/s_axis_tx_metadata]
connect_bd_intf_net [get_bd_intf_pins tcp_out_0/m_axis_tx_data] [get_bd_intf_pins tcp_ip_wrapper_0/s_axis_tx_data]

connect_bd_intf_net [get_bd_intf_pins M00_AXI] [get_bd_intf_pins tcp_ip_wrapper_0/M00_AXI]
connect_bd_intf_net [get_bd_intf_pins m_axi_Data_out] [get_bd_intf_pins tcp_in_0/m_axi_Data_out_V]
connect_bd_intf_net [get_bd_intf_pins m_axi_Data_in] [get_bd_intf_pins tcp_out_0/m_axi_Data_in_V]
connect_bd_net [get_bd_pins clk_161] [get_bd_pins network_module/clk_161]

connect_bd_intf_net [get_bd_intf_pins m_axi_Data_out_big] [get_bd_intf_pins tcp_in_0/m_axi_Data_out_big_V]
connect_bd_intf_net [get_bd_intf_pins m_axi_Data_in_mem] [get_bd_intf_pins tcp_out_0/m_axi_Data_in_mem_V]

connect_bd_intf_net [get_bd_intf_pins M00_AXI] [get_bd_intf_pins tcp_ip_wrapper_0/M00_AXI]

connect_bd_net [get_bd_pins tmp_mems_c/dout] [get_bd_pins tcp_out_0/num_of_mems]
connect_bd_net [get_bd_pins BRAM_size] [get_bd_pins tcp_out_0/mem_size]
###

current_bd_instance network_module

connect_bd_net [get_bd_pins refclk_p] [get_bd_pins xxv_ethernet_0/gt_refclk_p]
connect_bd_net [get_bd_pins refclk_n] [get_bd_pins xxv_ethernet_0/gt_refclk_n]
connect_bd_intf_net [get_bd_intf_pins gt_rx] [get_bd_intf_pins xxv_ethernet_0/gt_rx]

connect_bd_net [get_bd_pins vcc/dout] [get_bd_pins xxv_ethernet_0/ctl_tx_enable_0]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_fcs_ins_enable_0] [get_bd_pins vcc/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_check_preamble_0] [get_bd_pins vcc/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_check_sfd_0] [get_bd_pins vcc/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_delete_fcs_0] [get_bd_pins vcc/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_enable_0] [get_bd_pins vcc/dout]

connect_bd_net [get_bd_pins gnd/dout] [get_bd_pins xxv_ethernet_0/ctl_tx_data_pattern_select_0]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_ignore_fcs_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_send_idle_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_send_lfi_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_send_rfi_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_test_pattern_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_test_pattern_enable_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_test_pattern_select_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_force_resync_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins gnd/dout] [get_bd_pins xxv_ethernet_0/ctl_rx_ignore_fcs_0]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_data_pattern_select_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_process_lfi_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins gnd/dout] [get_bd_pins xxv_ethernet_0/ctl_rx_test_pattern_enable_0]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_rx_test_pattern_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/gtwiz_reset_tx_datapath_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/gtwiz_reset_rx_datapath_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/rx_reset_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins xxv_ethernet_0/tx_reset_0] [get_bd_pins gnd/dout]
connect_bd_net [get_bd_pins seed_c/dout] [get_bd_pins xxv_ethernet_0/ctl_tx_test_pattern_seed_a_0]
connect_bd_net [get_bd_pins xxv_ethernet_0/ctl_tx_test_pattern_seed_b_0] [get_bd_pins seed_c/dout]

connect_bd_net [get_bd_pins max_pack_l/dout] [get_bd_pins xxv_ethernet_0/ctl_rx_max_packet_len_0]
connect_bd_net [get_bd_pins min_pack_l/dout] [get_bd_pins xxv_ethernet_0/ctl_rx_min_packet_len_0]
connect_bd_net [get_bd_pins loop_c/dout] [get_bd_pins xxv_ethernet_0/gt_loopback_in_0]

connect_bd_net [get_bd_pins tx_pre_c/dout] [get_bd_pins xxv_ethernet_0/tx_preamblein_0]
connect_bd_net [get_bd_pins transc_clk_sel/dout] [get_bd_pins xxv_ethernet_0/txoutclksel_in_0]
connect_bd_net [get_bd_pins xxv_ethernet_0/rxoutclksel_in_0] [get_bd_pins transc_clk_sel/dout]

connect_bd_net [get_bd_pins dlck_100MHz] [get_bd_pins xxv_ethernet_0/dclk]
connect_bd_net [get_bd_pins network_reset] [get_bd_pins xxv_ethernet_0/sys_reset]
connect_bd_net [get_bd_pins xxv_ethernet_0/tx_clk_out_0] [get_bd_pins xxv_ethernet_0/rx_core_clk_0]

connect_bd_intf_net [get_bd_intf_pins gt_tx] [get_bd_intf_pins xxv_ethernet_0/gt_tx]

connect_bd_intf_net [get_bd_intf_pins s_axis_tx] [get_bd_intf_pins axis_register_slice_in/S_AXIS]
connect_bd_intf_net [get_bd_intf_pins axis_register_slice_in/M_AXIS] [get_bd_intf_pins fifo_generator_0/S_AXIS]
connect_bd_intf_net [get_bd_intf_pins fifo_generator_0/M_AXIS] [get_bd_intf_pins xxv_ethernet_0/axis_tx_0]

connect_bd_net [get_bd_pins fifo_generator_0/s_aclk] [get_bd_pins xxv_ethernet_0/tx_clk_out_0]
connect_bd_net [get_bd_pins axis_register_slice_out/aclk] [get_bd_pins xxv_ethernet_0/tx_clk_out_0]
connect_bd_net [get_bd_pins axis_register_slice_in/aclk] [get_bd_pins xxv_ethernet_0/tx_clk_out_0]

connect_bd_net [get_bd_pins axis_aresetn] [get_bd_pins axis_register_slice_in/aresetn]
connect_bd_net [get_bd_pins axis_aresetn] [get_bd_pins fifo_generator_0/s_aresetn]
connect_bd_net [get_bd_pins axis_aresetn] [get_bd_pins axis_register_slice_out/aresetn]

connect_bd_intf_net [get_bd_intf_pins m_axis_rx] [get_bd_intf_pins axis_register_slice_out/M_AXIS]
connect_bd_intf_net [get_bd_intf_pins xxv_ethernet_0/axis_rx_0] [get_bd_intf_pins axis_register_slice_out/S_AXIS]

connect_bd_net [get_bd_pins xxv_ethernet_0/user_tx_reset_0] [get_bd_pins user_reset_and/Op2]
connect_bd_net [get_bd_pins user_reset_and/Res] [get_bd_pins reset_inv/Op1]
connect_bd_net [get_bd_pins net_init_done] [get_bd_pins reset_inv/Res]

connect_bd_net [get_bd_pins clk_161] [get_bd_pins xxv_ethernet_0/tx_clk_out_0]

current_bd_instance
#
###

connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins hbm_inst/AXI_14_ARESET_N]                     

connect_bd_net -net refclk300_n_1 [get_bd_ports refclk300_n] [get_bd_pins refclk_ibufds/IBUF_DS_N]
connect_bd_net -net refclk300_p_1 [get_bd_ports refclk300_p] [get_bd_pins refclk_ibufds/IBUF_DS_P]

connect_bd_net [get_bd_pins refclk_ibufds/IBUF_OUT] [get_bd_pins clk_wiz_0/clk_in1]
connect_bd_net [get_bd_pins refclk_ibufds/IBUF_OUT] [get_bd_pins refclk_bufg/BUFG_I]

connect_bd_net [get_bd_pins logic_reset_op/Op1] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_net [get_bd_pins psreset_data/slowest_sync_clk] [get_bd_pins refclk_bufg/BUFG_O]

>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_read_X/ARESETN]
>> rep2 10 6 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_read_X/M0Y_ARESETN]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_read_X/S00_ARESETN]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_read_X/S01_ARESETN]

>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_write_X/ARESETN]
>> rep2 10 7 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_write_X/M0Y_ARESETN]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_write_X/S00_ARESETN]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_bram_write_X/S01_ARESETN]

#connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/aresetn]
#connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_memory_A/S00_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M00_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M01_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M02_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M03_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M04_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M05_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M06_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M07_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M08_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M09_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M10_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M11_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M12_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/M13_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/S00_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_memory_A/S01_ARESETN]

>> rep2 10 4 connect_bd_intf_net [get_bd_intf_pins reg_writer_X_Y/M_AXI_BUS_IF] [get_bd_intf_pins xcon_bram_write_X/S00_AXI]

connect_bd_net [get_bd_pins refclk_bufg/BUFG_O]\
>> rep 10 [get_bd_pins xcon_bram_read_X/ACLK]\
>> rep 10 [get_bd_pins xcon_bram_write_X/ACLK]\
[get_bd_pins xcon_memory_A/ACLK]\
[get_bd_pins xcon_memory_A/M00_ACLK]\
[get_bd_pins xcon_memory_A/M01_ACLK]\
[get_bd_pins xcon_memory_A/M02_ACLK]\
[get_bd_pins xcon_memory_A/M03_ACLK]\
[get_bd_pins xcon_memory_A/M04_ACLK]\
[get_bd_pins xcon_memory_A/M05_ACLK]\
[get_bd_pins xcon_memory_A/M06_ACLK]\
[get_bd_pins xcon_memory_A/M07_ACLK]\
[get_bd_pins xcon_memory_A/M08_ACLK]\
[get_bd_pins xcon_memory_A/M09_ACLK]\
[get_bd_pins xcon_memory_A/M10_ACLK]\
[get_bd_pins xcon_memory_A/M11_ACLK]\
[get_bd_pins xcon_memory_A/M12_ACLK]\
[get_bd_pins xcon_memory_A/M13_ACLK]\
[get_bd_pins xcon_memory_A/S00_ACLK]\
[get_bd_pins xcon_memory_A/S01_ACLK]\
 [get_bd_pins parse_arb_A/clk]\
>> rep 1 [get_bd_pins parse_X/clk]\
>> rep 1 [get_bd_pins hash_arb_X/clk]\
>> rep2 1 2 [get_bd_pins hash_gen_X_Y/clk]\
>> rep2 1 3 [get_bd_pins channel_sel_X_Y/clk]\
>> rep 1 [get_bd_pins channel_arb_X/clk]\
>> rep2 10 5 [get_bd_pins reg_writer_X_Y/aclk]\
>> rep2 10 5 [get_bd_pins big_request_writer_X_Y/aclk]\
>> rep2 10 4 [get_bd_pins read_req_writer_X_Y/aclk]\
>> rep 10 [get_bd_pins memory_writer_X/aclk]\
>> rep 10 [get_bd_pins memory_reader_X/aclk]\
>> rep 10 [get_bd_pins reg_arb_R_X/clk]\
>> rep 10 [get_bd_pins reg_arb_W_X/clk]\
>> rep 10 [get_bd_pins reg_arb_BW_X/clk]\
[get_bd_pins hbm_inst/AXI_00_ACLK]\
[get_bd_pins hbm_inst/AXI_01_ACLK]\
[get_bd_pins hbm_inst/AXI_02_ACLK]\
[get_bd_pins hbm_inst/AXI_03_ACLK]\
[get_bd_pins hbm_inst/AXI_04_ACLK]\
[get_bd_pins hbm_inst/AXI_05_ACLK]\
[get_bd_pins hbm_inst/AXI_06_ACLK]\
[get_bd_pins hbm_inst/AXI_07_ACLK]\
[get_bd_pins hbm_inst/AXI_08_ACLK]\
[get_bd_pins hbm_inst/AXI_09_ACLK]\
[get_bd_pins hbm_inst/AXI_10_ACLK]\
[get_bd_pins hbm_inst/AXI_11_ACLK]\
[get_bd_pins hbm_inst/AXI_12_ACLK]\
[get_bd_pins hbm_inst/AXI_13_ACLK]\
>> rep2 10 6 [get_bd_pins axi_bram_read_ctrl_X_Y/s_axi_aclk]\
>> rep2 10 7 [get_bd_pins axi_bram_write_ctrl_X_Y/s_axi_aclk]\
>> rep 10 [get_bd_pins xcon_bram_read_X/S00_ACLK]\
>> rep 10 [get_bd_pins xcon_bram_read_X/S01_ACLK]\
>> rep 10 [get_bd_pins xcon_bram_write_X/S00_ACLK]\
>> rep 10 [get_bd_pins xcon_bram_write_X/S01_ACLK]\
>> rep2 10 6 [get_bd_pins xcon_bram_read_X/M0Y_ACLK]\
>> rep2 10 7 [get_bd_pins xcon_bram_write_X/M0Y_ACLK]\

#[get_bd_pins xcon_memory_A/aclk]\

connect_bd_net [get_bd_ports sys_rst_n] [get_bd_pins psreset_data/ext_reset_in]

#>> rep 0 connect_bd_net [get_bd_pins hbm_inst/apb_complete_0] [get_bd_pins req_gen_X/start]

#>> rep 0 connect_bd_net [get_bd_pins parse_arb_A/free_Z] [get_bd_pins req_gen_X/free]

#>> rep 0 connect_bd_net [get_bd_pins parse_arb_A/done_out_prev_Z] [get_bd_pins req_gen_X/done_in]

#>> rep 0 connect_bd_net [get_bd_pins req_gen_X/enable_out] [get_bd_pins parse_arb_A/enable_in_Z]

#>> rep 0 connect_bd_net [get_bd_pins req_gen_X/valid_req] [get_bd_pins parse_arb_A/valid_req_Z]

#>> rep 0 connect_bd_net [get_bd_pins req_gen_X/req_out] [get_bd_pins parse_arb_A/req_in_Z]

>> rep 1 connect_bd_net [get_bd_pins parse_X/done_out] [get_bd_pins parse_arb_A/done_parser_Z]

>> rep 1 connect_bd_net [get_bd_pins parse_X/ready_out] [get_bd_pins parse_arb_A/parser_ready_Z]

>> rep 1 connect_bd_net [get_bd_pins parse_arb_A/valid_out_Z] [get_bd_pins parse_X/enable_in]

>> rep 1 connect_bd_net [get_bd_pins parse_arb_A/req_out_Z] [get_bd_pins parse_X/data]

>> rep 1 connect_bd_net [get_bd_pins parse_X/req_done] [get_bd_pins parse_arb_A/req_done_Z]

#>> rep 0 connect_bd_net [get_bd_pins req_gen_X/reset] [get_bd_pins logic_reset_op/Res]

connect_bd_net [get_bd_pins logic_reset_op/Res] [get_bd_pins parse_arb_A/reset]

>> rep 1 connect_bd_net [get_bd_pins parse_X/num_of_channels] [get_bd_pins num_channels_c_X/dout]

>> rep 1 connect_bd_net [get_bd_pins logic_reset_op/Res] [get_bd_pins parse_X/reset]
>> rep2 1 2 connect_bd_net [get_bd_pins logic_reset_op/Res] [get_bd_pins hash_gen_X_Y/reset]
>> rep 1 connect_bd_net [get_bd_pins logic_reset_op/Res] [get_bd_pins hash_arb_X/reset]
>> rep2 1 3 connect_bd_net [get_bd_pins logic_reset_op/Res] [get_bd_pins channel_sel_X_Y/reset]
>> rep 1 connect_bd_net [get_bd_pins logic_reset_op/Res] [get_bd_pins channel_arb_X/reset]


#connect_bd_net -net state_out [get_bd_pins parse_A/state_out]

>> rep2 10 6 connect_bd_intf_net [get_bd_intf_pins axi_bram_read_ctrl_X_Y/BRAM_PORTA] [get_bd_intf_pins bram_read_X_Y/BRAM_PORTA]
>> rep2 10 6 connect_bd_intf_net [get_bd_intf_pins axi_bram_read_ctrl_X_Y/BRAM_PORTB] [get_bd_intf_pins bram_read_X_Y/BRAM_PORTB]
>> rep2 10 7 connect_bd_intf_net [get_bd_intf_pins axi_bram_write_ctrl_X_Y/BRAM_PORTA] [get_bd_intf_pins bram_write_X_Y/BRAM_PORTA]
>> rep2 10 7 connect_bd_intf_net [get_bd_intf_pins axi_bram_write_ctrl_X_Y/BRAM_PORTB] [get_bd_intf_pins bram_write_X_Y/BRAM_PORTB]
>> rep2 10 7 connect_bd_intf_net [get_bd_intf_pins axi_bram_write_ctrl_X_Y/S_AXI] [get_bd_intf_pins xcon_bram_write_X/M0Y_AXI]

>> rep2 10 6 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins axi_bram_read_ctrl_X_Y/s_axi_aresetn]
>> rep2 10 6 connect_bd_intf_net [get_bd_intf_pins axi_bram_read_ctrl_X_Y/S_AXI] [get_bd_intf_pins xcon_bram_read_X/M0Y_AXI]
>> rep2 10 7 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins axi_bram_write_ctrl_X_Y/s_axi_aresetn]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_00_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_01_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_02_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_03_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_04_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_05_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_06_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_07_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_08_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_09_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_10_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_11_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_12_ARESET_N]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins hbm_inst/AXI_13_ARESET_N]

connect_bd_net [get_bd_ports sys_rst_n] [get_bd_pins clk_wiz_0/resetn]
connect_bd_net [get_bd_ports sys_rst_n] [get_bd_pins hbm_inst/APB_0_PRESET_N]
connect_bd_net [get_bd_ports sys_rst_n] [get_bd_pins hbm_inst/APB_1_PRESET_N]

connect_bd_net [get_bd_pins zero1_inst/dout] [get_bd_pins hbm_inst/APB_0_PENABLE]
connect_bd_net [get_bd_pins zero1_inst/dout] [get_bd_pins hbm_inst/APB_0_PSEL]
connect_bd_net [get_bd_pins zero1_inst/dout] [get_bd_pins hbm_inst/APB_0_PWRITE]
connect_bd_net [get_bd_pins zero1_inst/dout] [get_bd_pins hbm_inst/APB_1_PENABLE]
connect_bd_net [get_bd_pins zero1_inst/dout] [get_bd_pins hbm_inst/APB_1_PSEL]
connect_bd_net [get_bd_pins zero1_inst/dout] [get_bd_pins hbm_inst/APB_1_PWRITE]

connect_bd_net [get_bd_pins zero22_inst/dout] [get_bd_pins hbm_inst/APB_0_PADDR]
connect_bd_net [get_bd_pins zero22_inst/dout] [get_bd_pins hbm_inst/APB_1_PADDR]

connect_bd_net [get_bd_pins zero32_inst/dout] [get_bd_pins hbm_inst/APB_0_PWDATA]
connect_bd_net [get_bd_pins zero32_inst/dout] [get_bd_pins hbm_inst/APB_1_PWDATA]

connect_bd_net [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins hbm_inst/HBM_REF_CLK_0]
connect_bd_net [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins hbm_inst/HBM_REF_CLK_1]

connect_bd_net [get_bd_pins clk_wiz_0/clk_out2] [get_bd_pins hbm_inst/APB_0_PCLK]
connect_bd_net [get_bd_pins clk_wiz_0/clk_out2] [get_bd_pins hbm_inst/APB_1_PCLK]

>> rep2 1 10 connect_bd_net [get_bd_pins size_limit_Z/dout] [get_bd_pins enable_logic_X_Y/size_limit]
>> rep 10 connect_bd_net [get_bd_pins size_limit_X/dout] [get_bd_pins memory_reader_X/Big_req]

>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/size_out] [get_bd_pins enable_logic_X_Y/size_in]

>> rep2 1 2 connect_bd_net [get_bd_pins hash_gen_X_Y/seed] [get_bd_pins seed_c_X_Y/dout]
>> rep2 1 2 connect_bd_net [get_bd_pins channel_sel_X_Y/seed] [get_bd_pins seed_ch_c_X_Y/dout]
>> rep 1 connect_bd_net [get_bd_pins parse_X/key_l_out] [get_bd_pins hash_arb_X/key_len_in]
>> rep 1 connect_bd_net [get_bd_pins parse_X/key_l_out] [get_bd_pins channel_arb_X/key_len_in]
>> rep2 1 3 connect_bd_net [get_bd_pins hash_reducer_X_Y/free_out] [get_bd_pins channel_arb_X/hash_ready_Y]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_arb_X/key_len_out] [get_bd_pins hash_gen_X_Y/key_len] 
>> rep2 1 2 connect_bd_net [get_bd_pins channel_arb_X/key_len_out] [get_bd_pins channel_sel_X_Y/key_len]

>> rep 1 connect_bd_net [get_bd_pins parse_X/hash_enable] [get_bd_pins hash_arb_X/enable_in]
>> rep 1 connect_bd_net [get_bd_pins parse_X/key_out] [get_bd_pins hash_arb_X/key_in]
>> rep 1 connect_bd_net [get_bd_pins parse_X/hash_in] [get_bd_pins hash_arb_X/hash_out]
>> rep 1 connect_bd_net [get_bd_pins parse_X/hash_done] [get_bd_pins hash_arb_X/done_out_prev]

>> rep2 1 2 connect_bd_net [get_bd_pins hash_arb_X/enable_out] [get_bd_pins hash_gen_X_Y/enable_in]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_arb_X/hash_ready_Y] [get_bd_pins hash_gen_X_Y/free_out]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_gen_X_Y/data_out] [get_bd_pins hash_rd_X_Y/hash_in]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_arb_X/hash_in_Y] [get_bd_pins hash_rd_X_Y/hash_rd_out]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_arb_X/key_out] [get_bd_pins hash_gen_X_Y/data_in]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_gen_X_Y/done_out] [get_bd_pins hash_rd_X_Y/hash_in_valid]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_rd_X_Y/hash_out_valid] [get_bd_pins hash_arb_X/done_from_hash]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_rd_X_Y/rst] [get_bd_pins logic_reset_op/Res]
>> rep2 1 2 connect_bd_net [get_bd_pins hash_rd_X_Y/clk] [get_bd_pins refclk_bufg/BUFG_O]


>> rep2 1 3 connect_bd_net [get_bd_pins channel_arb_X/enable_out] [get_bd_pins channel_sel_X_Y/enable_in]
>> rep2 1 3 connect_bd_net [get_bd_pins channel_arb_X/key_out] [get_bd_pins channel_sel_X_Y/data_in]

>> rep2 1 2 connect_bd_net [get_bd_pins channel_sel_X_Y/done_out] [get_bd_pins hash_reducer_X_Y/done_in]
>> rep2 1 3 connect_bd_net [get_bd_pins channel_sel_X_Y/free_out] [get_bd_pins hash_reducer_X_Y/free_in]
>> rep2 1 3 connect_bd_net [get_bd_pins channel_sel_X_Y/data_out] [get_bd_pins hash_reducer_X_Y/hash_in]

>> rep2 1 3 connect_bd_net [get_bd_pins hash_reducer_X_Y/hash_out] [get_bd_pins channel_arb_X/hash_in_Y] 
>> rep2 1 3 connect_bd_net [get_bd_pins hash_reducer_X/done_out] [get_bd_pins channel_arb_X/done_from_hash_Y]

>> rep2 1 3 connect_bd_net [get_bd_pins reducer_mode_c/dout] [get_bd_pins hash_reducer_X_Y/sel_size]


>> rep 1 connect_bd_net [get_bd_pins parse_X/hash_enable] [get_bd_pins channel_arb_X/enable_in]
>> rep 1 connect_bd_net [get_bd_pins parse_X/key_out] [get_bd_pins channel_arb_X/key_in]
>> rep 1 connect_bd_net [get_bd_pins parse_X/channel_done] [get_bd_pins channel_arb_X/done_out_prev]
>> rep 1 connect_bd_net [get_bd_pins parse_X/channel_in] [get_bd_pins channel_arb_X/hash_out]

>> rep 10 connect_bd_net [get_bd_pins memory_writer_X/aresetn] [get_bd_pins psreset_data/interconnect_aresetn]
>> rep2 10 5 connect_bd_net [get_bd_pins memory_writer_X/free_reg_0] [get_bd_pins reg_writer_X_Y/free_reg_0]
>> rep2 10 5 connect_bd_net [get_bd_pins memory_writer_X/free_reg_1] [get_bd_pins reg_writer_X_Y/free_reg_1]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_writer_X_Y/valid_out_0] [get_bd_pins memory_writer_X/valid_reg_0]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_writer_X_Y/valid_out_1] [get_bd_pins memory_writer_X/valid_reg_1]

>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_writer_X/M_AXI_BUS_READ] [get_bd_intf_pins xcon_bram_write_X/S01_AXI]

#connect_bd_intf_net [get_bd_intf_pins TCP_Subsystem/m_axi_Data_in_mem] [get_bd_intf_pins xcon_memory_A/S00_AXI]
#connect_bd_intf_net [get_bd_intf_pins xcon_write_mem_A/M00_AXI] [get_bd_intf_pins xcon_memory_A/S01_AXI]
#connect_bd_intf_net [get_bd_intf_pins xcon_big_mem_A/M00_AXI] [get_bd_intf_pins xcon_memory_A/S02_AXI]
#connect_bd_intf_net [get_bd_intf_pins xcon_read_mem_A/M00_AXI] [get_bd_intf_pins xcon_memory_A/S03_AXI]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins xcon_memory_0/M00_AXI] [get_bd_intf_pins xcon_memory_A/S00_AXI]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins xcon_memory_1/M00_AXI] [get_bd_intf_pins xcon_memory_A/S01_AXI]

>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_writer_X/M_AXI_BUS_WRITE] [get_bd_intf_pins xcon_write_mem_A/S0Z_AXI]
>> rep 10 connect_bd_intf_net [get_bd_intf_pins big_request_writer_X/M_AXI_BUS_WRITE] [get_bd_intf_pins xcon_big_mem_A/S0Z_AXI]
>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_reader_X/M_AXI_BUS_READ_MEM] [get_bd_intf_pins xcon_read_mem_A/S0Z_AXI]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_big_mem_A/ACLK]
>> rep 10 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_big_mem_A/S0Z_ACLK]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_big_mem_A/M00_ACLK]

connect_bd_net [get_bd_pins xcon_big_mem_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
#connect_bd_net [get_bd_pins xcon_big_mem_A/S00_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
>> rep 10 connect_bd_net [get_bd_pins xcon_big_mem_A/S0Z_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_big_mem_A/M00_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
#connect_bd_net [get_bd_pins xcon_read_mem_A/aclk] [get_bd_pins refclk_bufg/BUFG_O]
#connect_bd_net [get_bd_pins xcon_read_mem_A/aresetn] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_read_mem_A/ACLK] 
>> rep 10 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_read_mem_A/S0Z_ACLK]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_read_mem_A/M00_ACLK]
connect_bd_net [get_bd_pins xcon_write_mem_A/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
>> rep 10 connect_bd_net [get_bd_pins xcon_write_mem_A/S0Z_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_write_mem_A/M00_ACLK] [get_bd_pins refclk_bufg/BUFG_O]

connect_bd_net [get_bd_pins xcon_write_mem_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
>> rep 10 connect_bd_net [get_bd_pins xcon_write_mem_A/S0Z_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_write_mem_A/M00_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_read_mem_A/ARESETN]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_read_mem_A/S0Z_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_read_mem_A/M00_ARESETN]

#>> rep2 10 5 connect_bd_intf_net [get_bd_intf_pins big_request_writer_X_Y/M_AXI_BUS_WRITE] [get_bd_intf_pins xcon_memory_X/S02_AXI]
>> rep2 10 5 connect_bd_net [get_bd_pins mem_base_address_X/dout] [get_bd_pins big_request_writer_X_Y/base_address]

>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_reader_X/M_AXI_BUS_READ_REG] [get_bd_intf_pins xcon_bram_read_X/S01_AXI]

>> rep 10 connect_bd_net [get_bd_pins mem_base_address_X/dout] [get_bd_pins memory_writer_X/addr_base]
>> rep 10 connect_bd_net [get_bd_pins mem_base_address_X/dout] [get_bd_pins memory_reader_X/addr_base]

>> rep2 10 5 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins reg_writer_X_Y/aresetn]
>> rep2 10 5 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins big_request_writer_X_Y/aresetn]
>> rep2 10 4 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins read_req_writer_X_Y/aresetn]

>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins memory_reader_X/aresetn]

>> rep2 1 10 connect_bd_net [get_bd_pins read_req_wrtr_or_X_Y/Op2] [get_bd_pins enable_logic_X_Y/enable_hi_out]

>> rep2 1 10 connect_bd_net [get_bd_pins enable_logic_X_Y/valid] [get_bd_pins parse_X/valid_out]
>> rep2 1 10 connect_bd_net [get_bd_pins enable_const_X_Y/dout] [get_bd_pins enable_logic_X_Y/ch_self]
>> rep 1 connect_bd_net [get_bd_pins parse_X/channel_out] [get_bd_pins channel_mask_and_X/Op1]
>> rep 1 connect_bd_net [get_bd_pins channel_mask_X/dout] [get_bd_pins channel_mask_and_X/Op2]
>> rep2 1 10 connect_bd_net [get_bd_pins channel_mask_and_X/Res] [get_bd_pins enable_logic_X_Y/ch_in]

>> rep2 10 4 connect_bd_net [get_bd_pins read_req_writer_X_Y/free_reg_0] [get_bd_pins memory_reader_X/free_reg_0]
>> rep2 10 4 connect_bd_net [get_bd_pins read_req_writer_X_Y/free_reg_1] [get_bd_pins memory_reader_X/free_reg_1]
>> rep2 10 4 connect_bd_intf_net [get_bd_intf_pins xcon_bram_read_X/S00_AXI] [get_bd_intf_pins read_req_writer_X_Y/M_AXI_BUS_IF]
>> rep2 10 4 connect_bd_net [get_bd_pins memory_reader_X/valid_reg_0] [get_bd_pins read_req_writer_X_Y/valid_out_0]
>> rep2 10 4 connect_bd_net [get_bd_pins read_req_writer_X_Y/valid_out_1] [get_bd_pins memory_reader_X/valid_reg_1]

>> rep2 1 10 connect_bd_net [get_bd_pins ready_in_w_or_Z_Q/Res] [get_bd_pins parse_X/ready_in_w_Y]
#>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_reader_X/M_AXI_BUS_READ_MEM] [get_bd_intf_pins xcon_memory_X/S01_AXI]

>> rep 10 connect_bd_net [get_bd_pins register_offset_X/dout] [get_bd_pins memory_reader_X/register_words]
>> rep2 10 4 connect_bd_net [get_bd_pins read_req_writer_X_Y/register_words] [get_bd_pins register_offset_X/dout]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_writer_X_Y/register_words] [get_bd_pins register_offset_X/dout]
>> rep 10 connect_bd_net [get_bd_pins memory_writer_X/register_words] [get_bd_pins register_offset_X/dout]
#>> rep 0 connect_bd_net [get_bd_pins request_words_X/dout] [get_bd_pins req_gen_X/request_words]

connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M00_AXI] [get_bd_intf_pins hbm_inst/SAXI_00]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M01_AXI] [get_bd_intf_pins hbm_inst/SAXI_01]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M02_AXI] [get_bd_intf_pins hbm_inst/SAXI_02]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M03_AXI] [get_bd_intf_pins hbm_inst/SAXI_03]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M04_AXI] [get_bd_intf_pins hbm_inst/SAXI_04]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M05_AXI] [get_bd_intf_pins hbm_inst/SAXI_05]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M06_AXI] [get_bd_intf_pins hbm_inst/SAXI_06]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M07_AXI] [get_bd_intf_pins hbm_inst/SAXI_07]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M08_AXI] [get_bd_intf_pins hbm_inst/SAXI_08]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M09_AXI] [get_bd_intf_pins hbm_inst/SAXI_09]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M10_AXI] [get_bd_intf_pins hbm_inst/SAXI_10]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M11_AXI] [get_bd_intf_pins hbm_inst/SAXI_11]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M12_AXI] [get_bd_intf_pins hbm_inst/SAXI_12]
connect_bd_intf_net [get_bd_intf_pins xcon_memory_A/M13_AXI] [get_bd_intf_pins hbm_inst/SAXI_13]

>> rep 10 connect_bd_net [get_bd_pins reg_arb_R_X/reset] [get_bd_pins logic_reset_op/Res]
>> rep 10 connect_bd_net [get_bd_pins reg_arb_W_X/reset] [get_bd_pins logic_reset_op/Res] 
>> rep 10 connect_bd_net [get_bd_pins reg_arb_BW_X/reset] [get_bd_pins logic_reset_op/Res]

>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_W_and_X_Y/Res] [get_bd_pins reg_arb_W_Z/enable_in_Q]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_W_X/valid_out_Y] [get_bd_pins reg_writer_X_Y/enable]    

>> rep2 10 4 connect_bd_net [get_bd_pins reg_arb_R_X/valid_out_Y] [get_bd_pins read_req_writer_X_Y/enable]

>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_BW_and_X_Y/Res] [get_bd_pins reg_arb_BW_Z/enable_in_Q]

>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_BW_X/valid_out_Y] [get_bd_pins big_request_writer_X_Y/enable]

>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/rw] [get_bd_pins reg_arb_R_Z/rw_in_Q] 
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/rw] [get_bd_pins reg_arb_W_Z/rw_in_Q]
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/rw] [get_bd_pins reg_arb_BW_Z/rw_in_Q] 

>> rep2 10 4 connect_bd_net [get_bd_pins reg_arb_R_X/rw_out_Y] [get_bd_pins read_req_writer_X_Y/rw]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_W_X/rw_out_Y] [get_bd_pins reg_writer_X_Y/rw]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_BW_X/rw_out_Y] [get_bd_pins big_request_writer_X_Y/rw]

>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/req_write] [get_bd_pins reg_arb_BW_Z/data_in_Q]
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/req_write] [get_bd_pins reg_arb_W_Z/data_in_Q]

>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_BW_X/data_out_Y] [get_bd_pins big_request_writer_X_Y/data_in]

>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_BW_X/address_out_Y] [get_bd_pins big_request_writer_X_Y/address_in]

>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_W_X/data_out_Y] [get_bd_pins reg_writer_X_Y/data_in]               
>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_W_X/address_out_Y] [get_bd_pins reg_writer_X_Y/address_in]         

#>> rep2 10 4 connect_bd_net [get_bd_pins reg_arb_R_X/address_out_Y] [get_bd_pins read_req_writer_X_Y/data_in]       
>> rep2 10 4 connect_bd_net [get_bd_pins reg_arb_R_X/address_out_Y] [get_bd_pins read_req_writer_X_Y/address_in]
>> rep2 10 4 connect_bd_net [get_bd_pins reg_arb_R_X/data_out_Y] [get_bd_pins read_req_writer_X_Y/data_in]

>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/hash_out] [get_bd_pins reg_arb_BW_Z/address_in_Q]
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/hash_out] [get_bd_pins reg_arb_W_Z/address_in_Q]                   

>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/req_write] [get_bd_pins reg_arb_R_Z/data_in_Q]
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/hash_out] [get_bd_pins reg_arb_R_Z/address_in_Q]
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/size_out] [get_bd_pins reg_arb_R_Z/size_in_Q]  
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/size_out] [get_bd_pins reg_arb_BW_Z/size_in_Q] 
>> rep2 1 10 connect_bd_net [get_bd_pins parse_X/size_out] [get_bd_pins reg_arb_W_Z/size_in_Q] 

>> rep2 10 4 connect_bd_net [get_bd_pins read_req_writer_X_Y/done_out] [get_bd_pins reg_arb_R_X/done_in_Y]
>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_R_Z/done_out_Q] [get_bd_pins parse_X/ready_in_r_Y]         
>> rep2 10 5 connect_bd_net [get_bd_pins reg_writer_X_Y/done_out] [get_bd_pins reg_arb_W_X/done_in_Y]
>> rep2 10 1 connect_bd_net [get_bd_pins reg_arb_W_X/done_out_Y] [get_bd_pins ready_in_w_or_X_Y/Op1]   
>> rep2 10 1 connect_bd_net [get_bd_pins reg_arb_BW_X/done_out_Y] [get_bd_pins ready_in_w_or_X_Y/Op2]   

>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_BW_X/size_out_Y] [get_bd_pins big_request_writer_X_Y/size_in]
>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_W_X/size_out_Y] [get_bd_pins reg_writer_X_Y/size_in] 

>> rep2 1 10 connect_bd_net [get_bd_pins parser_rw_not_X_Y/Op1] [get_bd_pins parse_X/rw]
>> rep2 1 10 connect_bd_net [get_bd_pins parser_rw_not_X_Y/Res] [get_bd_pins reg_arb_R_and_X_Y/Op1]
>> rep2 1 10 connect_bd_net [get_bd_pins read_req_wrtr_or_X_Y/Res] [get_bd_pins reg_arb_R_and_X_Y/Op2]
>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_R_Z/enable_in_Q] [get_bd_pins reg_arb_R_and_X_Y/Res]

>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_W_and_X_Y/Op1] [get_bd_pins parse_X/rw]
>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_W_and_X_Y/Op2] [get_bd_pins enable_logic_X_Y/enable_out]

>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_BW_and_X_Y/Op1] [get_bd_pins parse_X/rw]
>> rep2 1 10 connect_bd_net [get_bd_pins reg_arb_BW_and_X_Y/Op2] [get_bd_pins enable_logic_X_Y/enable_hi_out]


>> rep2 1 10 connect_bd_net [get_bd_pins read_req_wrtr_or_X_Y/Op1] [get_bd_pins enable_logic_X_Y/enable_out]

>> rep2 10 5 connect_bd_net [get_bd_pins reg_arb_BW_X/done_in_Y] [get_bd_pins big_request_writer_X_Y/done_out]

connect_bd_net [get_bd_pins count_start_c/dout] [get_bd_pins cycle_counter_0/count_start]
connect_bd_net [get_bd_pins counter_xor_c/dout] [get_bd_pins counter_xor/Op1]
connect_bd_net [get_bd_pins counter_inv/Res] [get_bd_pins counter_xor/Op2]
#connect_bd_net [get_bd_pins counter_inv/Op1] [get_bd_pins req_gen_A/times_out]
connect_bd_net [get_bd_pins counter_xor/Res] [get_bd_pins xor_reduce/Op1]
connect_bd_net [get_bd_pins xor_reduce/Res] [get_bd_pins cycle_counter_0/pause]
connect_bd_net [get_bd_pins cycle_counter_0/clk] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins cycle_counter_0/reset] [get_bd_pins logic_reset_op/Res]
#connect_bd_intf_net [get_bd_intf_pins smartconnect_0/M04_AXI] [get_bd_intf_pins axi_gpio_1/S_AXI]
#connect_bd_net [get_bd_pins axi_gpio_1/s_axi_aclk] [get_bd_pins refclk_bufg/BUFG_O]
#connect_bd_net [get_bd_pins axi_gpio_1/s_axi_aresetn] [get_bd_pins psreset_data/interconnect_aresetn]
#connect_bd_net [get_bd_pins axi_gpio_1/gpio_io_i] [get_bd_pins cycle_counter_0/count_out]
#connect_bd_net [get_bd_pins cycle_counter_0/start] [get_bd_pins axi_gpio_0/gpio2_io_o]

connect_bd_net [get_bd_pins mem_calib_and/Op1] [get_bd_pins hbm_inst/apb_complete_0]
connect_bd_net [get_bd_pins mem_calib_and/Op2] [get_bd_pins hbm_inst/apb_complete_1]
connect_bd_net [get_bd_pins mem_calib_and/Res] [get_bd_pins cycle_counter_0/start]

connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_tcp_bram_A/ACLK]

connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_tcp_bram_A/ARESETN]

connect_bd_intf_net [get_bd_intf_ports gt_rx] -boundary_type upper [get_bd_intf_pins TCP_Subsystem/gt_rx]
connect_bd_intf_net [get_bd_intf_ports gt_tx] -boundary_type upper [get_bd_intf_pins TCP_Subsystem/gt_tx]
connect_bd_net [get_bd_ports sfp0_refclk_n] [get_bd_pins TCP_Subsystem/sfp_clk_n]
connect_bd_net [get_bd_ports sfp0_refclk_p] [get_bd_pins TCP_Subsystem/sft_clk_p]

connect_bd_net [get_bd_ports sys_rst_n] [get_bd_pins TCP_Subsystem/aresetn]
connect_bd_net [get_bd_pins clk_wiz_0/locked] [get_bd_pins TCP_Subsystem/locked]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_100] [get_bd_pins clk_wiz_0/clk_out1]

connect_bd_net [get_bd_pins mem_calib_and/Res] [get_bd_pins TCP_Subsystem/hbm_calib]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins TCP_Subsystem/M00_AXI] [get_bd_intf_pins xcon_tcp_mem/S00_AXI]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_tcp_mem/S00_ACLK]
connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_tcp_mem/S00_ARESETN]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_tcp_mem/ACLK]
connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_tcp_mem/ARESETN]
connect_bd_intf_net [get_bd_intf_pins xcon_tcp_mem/M00_AXI] [get_bd_intf_pins hbm_inst/SAXI_14]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_tcp_mem/M00_ACLK]
connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_tcp_mem/M00_ARESETN]

connect_bd_net [get_bd_pins hbm_inst/AXI_14_ACLK] [get_bd_pins TCP_Subsystem/clk_161]

>> rep 0 connect_bd_net [get_bd_pins request_handler_X/aclk] [get_bd_pins refclk_bufg/BUFG_O]
>> rep 0 connect_bd_net [get_bd_pins request_handler_X/aresetn] [get_bd_pins psreset_data/interconnect_aresetn]
#>> rep 1 connect_bd_intf_net [get_bd_intf_pins xcon_req_done_0/M00_AXI] [get_bd_intf_pins request_handler_X/S_AXI_VLD]


>> repM1 0 connect_bd_intf_net [get_bd_intf_pins request_handler_X/M_AXI_REQ_IN] [get_bd_intf_pins xcon_tcp_bram_A/S0Z+_AXI]
>> repM1 0 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_tcp_bram_A/S0Z+_ACLK]
>> repM1 0 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_tcp_bram_A/S0Z+_ARESETN]
>> repM2 0 connect_bd_intf_net [get_bd_intf_pins request_handler_X/M_AXI_REQ_IN] [get_bd_intf_pins xcon_mem_temp_A/S0Z+_AXI]
>> repM2 0 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_mem_temp_A/S0Z+_ACLK]
>> repM2 0 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_mem_temp_A/S0Z+_ARESETN]

#connect_bd_net [get_bd_pins xcon_tcp_bram_A/aclk1] [get_bd_pins refclk_bufg/BUFG_O]

>> rep 0 connect_bd_net [get_bd_pins request_handler_X/free] [get_bd_pins parse_arb_A/free_Z]
>> rep 0 connect_bd_net [get_bd_pins request_handler_X/start] [get_bd_pins mem_calib_and/Res]
>> rep 0 connect_bd_net [get_bd_pins request_handler_X/done_in] [get_bd_pins parse_arb_A/done_out_prev_Z]
>> rep_hl 0 connect_bd_net [get_bd_pins req_h_mem_b/dout] [get_bd_pins request_handler_X/req_base]
>> rep_hh 0 connect_bd_net [get_bd_pins req_h_mem_m/dout] [get_bd_pins request_handler_X/req_base]

>> rep 0 connect_bd_net [get_bd_pins request_handler_X/enable_out] [get_bd_pins parse_arb_A/enable_in_Z]
>> rep 0 connect_bd_net [get_bd_pins request_handler_X/valid_req] [get_bd_pins parse_arb_A/valid_req_Z]
>> rep 0 connect_bd_net [get_bd_pins request_handler_X/req_out] [get_bd_pins parse_arb_A/req_in_Z]

#>> rep 0 connect_bd_net [get_bd_pins request_handler_X/times_out] [get_bd_pins counter_inv/Op1]

connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_req_A/ACLK]
connect_bd_net [get_bd_pins xcon_in_req_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_in_write_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_in_read_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_in_big_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

>> repMul2 connect_bd_net [get_bd_pins axi_bram_req_ctrl_X/s_axi_aclk] [get_bd_pins refclk_bufg/BUFG_O]
>> repMul2 connect_bd_net [get_bd_pins axi_bram_req_ctrl_X/s_axi_aresetn] [get_bd_pins psreset_data/interconnect_aresetn]
>> repMul2 connect_bd_intf_net [get_bd_intf_pins axi_bram_req_ctrl_X/BRAM_PORTA] [get_bd_intf_pins in_req_bram_X/BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins TCP_Subsystem/m_axi_Data_in] [get_bd_intf_pins xcon_in_req_A/S00_AXI]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_in_req_A/S00_ACLK]
connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_in_req_A/S00_ARESETN]

connect_bd_net [get_bd_pins xcon_in_write_A/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_in_read_A/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_in_big_A/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
>> repMul2 connect_bd_intf_net [get_bd_intf_pins xcon_in_req_A/M0Z_AXI] [get_bd_intf_pins axi_bram_req_ctrl_X/S_AXI]
>> repMul2 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_req_A/M0Z_ACLK]
>> repMul2 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_req_A/M0Z_ARESETN]

connect_bd_intf_net -boundary_type upper [get_bd_intf_pins TCP_Subsystem/m_axi_Data_out] [get_bd_intf_pins xcon_tcp_bram_A/S00_AXI]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_tcp_bram_A/S00_ACLK]
connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_tcp_bram_A/S00_ARESETN]
>> rep 8 connect_bd_intf_net [get_bd_intf_pins xcon_tcp_bram_A/M0Z_AXI] [get_bd_intf_pins axi_bram_tcp_ctrl_X/S_AXI]
>> rep 8 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_tcp_bram_A/M0Z_ACLK]
>> rep 8 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_tcp_bram_A/M0Z_ARESETN]
#connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_tcp_bram_A/M00_ACLK]
#connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_tcp_bram_A/M00_ARESETN]
>> rep 8 connect_bd_intf_net [get_bd_intf_pins axi_bram_tcp_ctrl_X/BRAM_PORTA] [get_bd_intf_pins tcp_bram_X/BRAM_PORTA]
>> rep 8 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins axi_bram_tcp_ctrl_X/s_axi_aclk]
>> rep 8 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins axi_bram_tcp_ctrl_X/s_axi_aresetn]

>> rep_hl 0 connect_bd_net [get_bd_pins handler_num_b_X/dout] [get_bd_pins request_handler_X/handler_num]
>> rep_hh 0 connect_bd_net [get_bd_pins handler_num_m_Z/dout] [get_bd_pins request_handler_X/handler_num]

>> rep_hl 0 connect_bd_net [get_bd_pins mem_size_b/dout] [get_bd_pins request_handler_X/mem_size]
>> rep_hh 0 connect_bd_net [get_bd_pins mem_size_m/dout] [get_bd_pins request_handler_X/mem_size]
>> rep 0 connect_bd_net [get_bd_pins num_mem_c/dout] [get_bd_pins request_handler_X/num_mem]

>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_writer_X/M_AXI_BUS_WRITE_BRAM] [get_bd_intf_pins xcon_in_write_A/SW_AXI]
>> rep 10 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_write_A/SW_ACLK]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_write_A/SW_ARESETN]
>> rep 10 connect_bd_intf_net [get_bd_intf_pins big_request_writer_X/M_AXI_BUS_WRITE_BRAM] [get_bd_intf_pins xcon_in_big_A/SB_AXI]
>> rep 10 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_big_A/SB_ACLK]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_big_A/SB_ARESETN]
>> rep 10 connect_bd_intf_net [get_bd_intf_pins memory_reader_X/M_AXI_BUS_WRITE_BRAM] [get_bd_intf_pins xcon_in_read_A/SR_AXI]
>> rep 10 connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_read_A/SR_ACLK]
>> rep 10 connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_read_A/SR_ARESETN]

>> rep 10 connect_bd_net [get_bd_pins req_base_w_c_X/dout] [get_bd_pins memory_writer_X/req_base]
>> rep 10 connect_bd_net [get_bd_pins req_base_b_c_X/dout] [get_bd_pins big_request_writer_X/req_base]
>> rep 10 connect_bd_net [get_bd_pins req_base_r_c_X/dout] [get_bd_pins memory_reader_X/req_base]

connect_bd_intf_net -boundary_type upper [get_bd_intf_pins TCP_Subsystem/m_axi_Data_out_big] [get_bd_intf_pins xcon_mem_temp_A/S00_AXI]
connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_mem_temp_A/S00_ACLK]
connect_bd_net [get_bd_pins TCP_Subsystem/network_reset] [get_bd_pins xcon_mem_temp_A/S00_ARESETN]
#connect_bd_net [get_bd_pins TCP_Subsystem/clk_161] [get_bd_pins xcon_memory_A/S00_ACLK]

connect_bd_net [get_bd_pins xcon_mem_temp_A/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
#connect_bd_net [get_bd_pins xcon_mem_temp_A/aclk1] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_mem_temp_A/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_intf_net [get_bd_intf_pins xcon_mem_temp_A/M00_AXI] [get_bd_intf_pins hbm_inst/SAXI_15]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_mem_temp_A/M00_ACLK]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_mem_temp_A/M00_ARESETN]

connect_bd_net [get_bd_pins hbm_inst/AXI_15_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins hbm_inst/AXI_15_ARESET_N] [get_bd_pins psreset_data/interconnect_aresetn]

set_property name clk_apb [get_bd_nets clk_wiz_0_clk_out2]

>> rep2 10 5 connect_bd_net [get_bd_pins tmp_mems_b_c/dout] [get_bd_pins big_request_writer_X_Y/num_of_mems]
>> rep 10 connect_bd_net [get_bd_pins tmp_mems_r_c/dout] [get_bd_pins memory_reader_X/num_of_mems]
>> rep 10 connect_bd_net [get_bd_pins tmp_mems_w_c/dout] [get_bd_pins memory_writer_X/num_of_mems]

>> rep 10 connect_bd_net [get_bd_pins own_big_w_c_X/dout] [get_bd_pins big_request_writer_X/own_num]
>> rep 10 connect_bd_net [get_bd_pins own_mem_r_c_X/dout] [get_bd_pins memory_reader_X/own_num]
>> rep 10 connect_bd_net [get_bd_pins own_mem_w_c_X/dout] [get_bd_pins memory_writer_X/own_num]

>> rep 10 connect_bd_net [get_bd_pins BRAM_size_c/dout] [get_bd_pins big_request_writer_X/mem_size]
>> rep 10 connect_bd_net [get_bd_pins BRAM_size_c/dout] [get_bd_pins memory_writer_X/mem_size]
>> rep 10 connect_bd_net [get_bd_pins BRAM_size_c/dout] [get_bd_pins memory_reader_X/mem_size] 

connect_bd_intf_net [get_bd_intf_pins xcon_in_write_A/M00_AXI] [get_bd_intf_pins xcon_in_req_A/S01_AXI]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_write_A/M00_ACLK]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_write_A/M00_ARESETN]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_req_A/S01_ACLK]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_req_A/S02_ACLK]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_req_A/S03_ACLK]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_req_A/S01_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_req_A/S02_ARESETN]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_req_A/S03_ARESETN]
connect_bd_intf_net [get_bd_intf_pins xcon_in_big_A/M00_AXI] [get_bd_intf_pins xcon_in_req_A/S02_AXI]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_big_A/M00_ACLK]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_big_A/M00_ARESETN]
connect_bd_intf_net [get_bd_intf_pins xcon_in_read_A/M00_AXI] [get_bd_intf_pins xcon_in_req_A/S03_AXI]
connect_bd_net [get_bd_pins refclk_bufg/BUFG_O] [get_bd_pins xcon_in_read_A/M00_ACLK]
connect_bd_net [get_bd_pins psreset_data/interconnect_aresetn] [get_bd_pins xcon_in_read_A/M00_ARESETN]

connect_bd_net [get_bd_pins TCP_Subsystem/BRAM_size] [get_bd_pins BRAM_size_c/dout]
connect_bd_net [get_bd_pins xcon_memory_0/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_0/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_net [get_bd_pins xcon_memory_1/ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_1/ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_net [get_bd_pins xcon_memory_0/S00_ACLK] [get_bd_pins TCP_Subsystem/clk_161] -boundary_type upper
connect_bd_net [get_bd_pins xcon_memory_0/S00_ARESETN] [get_bd_pins TCP_Subsystem/network_reset] -boundary_type upper

connect_bd_net [get_bd_pins xcon_memory_1/S00_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_1/S00_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_net [get_bd_pins xcon_memory_0/M00_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_0/M00_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_memory_0/S01_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_0/S01_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_net [get_bd_pins xcon_memory_1/M00_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_1/M00_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]
connect_bd_net [get_bd_pins xcon_memory_1/S01_ACLK] [get_bd_pins refclk_bufg/BUFG_O]
connect_bd_net [get_bd_pins xcon_memory_1/S01_ARESETN] [get_bd_pins psreset_data/interconnect_aresetn]

connect_bd_intf_net -boundary_type upper [get_bd_intf_pins TCP_Subsystem/m_axi_Data_in_mem] [get_bd_intf_pins xcon_memory_0/S00_AXI]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins xcon_write_mem_A/M00_AXI] [get_bd_intf_pins xcon_memory_0/S01_AXI]

connect_bd_intf_net -boundary_type upper [get_bd_intf_pins xcon_big_mem_A/M00_AXI] [get_bd_intf_pins xcon_memory_1/S00_AXI]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins xcon_read_mem_A/M00_AXI] [get_bd_intf_pins xcon_memory_1/S01_AXI]
connect_bd_net [get_bd_pins num_mem_c/dout] [get_bd_pins TCP_Subsystem/small_mems]
