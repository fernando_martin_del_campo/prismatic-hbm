set vivado_version [split [version -short] .]
set vivado_major [lindex $vivado_version 0]
set vivado_minor [lindex $vivado_version 1]

set launchDir [file dirname [file normalize [info script]]]
set sourcesDir ${launchDir}/../sources

   create_project Prismatic Prismatic_prj -part xcvu37p-fsvh2892-2-e-es1

if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}
set source_set [get_filesets sources_1]

set_property "top" "prismatic_hbm_design_wrapper" $source_set

# Add HDL source files
set hdl_files [list \
  [file normalize "$sourcesDir/prismatic_hbm_design_wrapper.v"] \
]
add_files -norecurse -fileset $source_set $hdl_files

import_files -fileset constrs_1 -norecurse ${sourcesDir}/constraints/bitstream_r1.xdc
import_files -fileset constrs_1 -norecurse ${sourcesDir}/constraints/refclk300.xdc
import_files -fileset constrs_1 -norecurse ${sourcesDir}/constraints/debug.xdc


# CHANGE DESIGN NAME HERE
set design_name prismatic_hbm_design

create_bd_design $design_name

