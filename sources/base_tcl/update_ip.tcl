set_property ip_repo_paths "${sourcesDir}/ip" [current_project]
update_ip_catalog
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_request_handler_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_parser_arbiter_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_parser_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_register_writer_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_big_request_writer_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_register_arbiter_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_read_req_writer_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_memory_writer_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_memory_reader_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_murmur3_f_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_hash_arbiter_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_channel_arbiter_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/xilinx_com_hls_hash64x_1_0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/enable_logic_3.0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/cycle_counter_1.0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/cycle_counter_128_1.0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/user.org_user_hash_reducer_1.0.zip" -repo_path "${sourcesDir}/ip"
update_ip_catalog -add_ip "${sourcesDir}/ip/user.org_user_hash_rd_1.0.zip" -repo_path "${sourcesDir}/ip"

### TCP related
#
set ip_list [ list \
dlyma.org_dlyma_bit_synchronizer_1.0.zip \
dlyma.org_dlyma_network_packet_fifo_rx_1.1.zip \
dlyma.org_dlyma_network_packet_fifo_tx_1.1.zip \
ethz.systems_hls_arp_server_1.0.zip \
ethz.systems_hls_arp_server_subnet_1.0.zip \
ethz.systems_hls_ip_handler_1.2.zip \
ethz.systems_hls_tcp_slowrttoe_1.6.zip \
ethz.systems_hls_toe_1.6.zip \
pcgroup_fthfour_tuple_hash_1_0.zip \
user.org_user_ip_constant_block_1.0.zip \
user.org_user_tcp_ip_wrapper_wrapper_1.0.zip \
xilinx.labs_hls_dhcp_client_1.05.zip \
xilinx.labs_hls_icmp_server_1.67.zip \
xilinx.labs_hls_mac_ip_encode_1.04.zip \
xilinx.labs_hls_udp_1.41.zip \
xilinx.labs_hls_udpAppMux_1.05.zip \
xilinx.labs_hls_udpLoopback_1.10.zip \
xilinx_com_hls_echo_server_application_Int_1_0.zip \
xilinx_com_hls_tcp_in_1_0.zip \
xilinx_com_hls_tcp_out_1_0.zip \
]

foreach ip $ip_list {
update_ip_catalog -add_ip "${sourcesDir}/ip/$ip" -repo_path "${sourcesDir}/ip"
}

#
###


#################################################  REQUEST GENERATOR CLOCK PORTS CHANGE #################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name request_generator_v1_0_project -directory \
   /tmp/request_generator_v1_0_project \
   ${sourcesDir}/ip/xilinx_com_hls_request_generator_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
                                                                                                                                        
   ipx::infer_bus_interface clock xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::infer_bus_interface reset xilinx.com:signal:reset_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

##################################################  PARSER ARBITER CLOCK PORTS CHANGE ###################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name parser_arbiter_project -directory \
   /tmp/parser_arbiter_project \
   ${sourcesDir}/ip/xilinx_com_hls_parser_arbiter_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
                                                                                                                                        
   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

######################################################  PARSER CLOCK PORTS CHANGE #######################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name parser_project -directory \
   /tmp/parser_project \
   ${sourcesDir}/ip/xilinx_com_hls_parser_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
                                                                                                                                        
   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

#######################################################  HASH CLOCK PORTS CHANGE ########################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name hash_project -directory \
   /tmp/hash_project \
   ${sourcesDir}/ip/xilinx_com_hls_murmur3_f_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
   ipx::edit_ip_in_project -upgrade true -name hash_project -directory \
   /tmp/hash_project \
   ${sourcesDir}/ip/xilinx_com_hls_hash64x_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

###################################################  HASH ARBITER CLOCK PORTS CHANGE ####################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name hash_arbiter_project -directory \
   /tmp/hash_arbiter_project \
   ${sourcesDir}/ip/xilinx_com_hls_hash_arbiter_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
                                                                                                                                        
   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

##################################################  CHANNEL ARBITER CLOCK PORTS CHANGE ##################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name channel_arbiter_project -directory \
   /tmp/channel_arbiter_project \
   ${sourcesDir}/ip/xilinx_com_hls_channel_arbiter_1_0/component.xml                       
                                                                                                                                        
   update_compile_order -fileset sources_1                                                                                              
                                                                                                                                        
   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]                                                   
   ipx::update_ip_instances -delete_project true                                                                                        
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

##################################################  REGISTER ARBITER CLOCK PORTS CHANGE #################################################
#                                                                                                                                       #
#                                                                                                                                       #
   ipx::edit_ip_in_project -upgrade true -name register_arbiter_project -directory \
   /tmp/channel_arbiter_project \
   ${sourcesDir}/ip/xilinx_com_hls_register_arbiter_1_0/component.xml
   
   update_compile_order -fileset sources_1

   ipx::infer_bus_interface clk xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
   ipx::infer_bus_interface reset xilinx.com:signal:reset_rtl:1.0 [ipx::current_core]
   ipx::update_ip_instances -delete_project true
#                                                                                                                                       #
#                                                                                                                                       #
#########################################################################################################################################

