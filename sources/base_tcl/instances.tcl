######################################################  CREATE THE MODULES  ######################################################
#                                                                                                                                #
#                                                                                                                                #

current_bd_instance


create_bd_cell -type hier TCP_Subsystem

current_bd_instance TCP_Subsystem

create_bd_pin -dir O network_reset

create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 psreset_data

create_bd_pin -dir O clk_161

create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI
create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_Data_out
create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_Data_in
create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_Data_out_big
create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_Data_in_mem

create_bd_intf_pin -mode Slave -vlnv xilinx.com:display_xxv_ethernet:gt_ports:2.0 gt_rx
create_bd_intf_pin -mode Master -vlnv xilinx.com:display_xxv_ethernet:gt_ports:2.0 gt_tx

create_bd_pin -dir I -type rst aresetn

create_bd_pin -dir I -type clock clk_100
create_bd_pin -dir I locked

create_bd_pin -dir I hbm_calib

create_bd_pin -dir I -type clock sfp_clk_n
create_bd_pin -dir I -type clock sft_clk_p
 
create_bd_pin -dir I BRAM_size

create_bd_pin -dir I -from 31 -to 0 small_mems

create_bd_cell -type ip -vlnv dlyma.org:dlyma:bit_synchronizer:1.0 bit_synchronizer_0

create_bd_cell -type ip -vlnv dlyma.org:dlyma:bit_synchronizer:1.0 bit_synchronizer_1

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0

create_bd_cell -type ip -vlnv xilinx.com:ip:util_reduced_logic:2.0 util_reduced_logic_0

create_bd_cell -type ip -vlnv user.org:user:ip_constant_block:1.0 ip_constant_block_0

create_bd_cell -type ip -vlnv user.org:user:tcp_ip_wrapper_wrapper:1.0 tcp_ip_wrapper_0

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 TCP_wrapper_c

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 TCP_wrapper_c33

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 arp_en_c

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 big_req_c

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 big_mems_c

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 tmp_mems_c

create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 reset_inv

create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 fifo_generator_0

create_bd_cell -type ip -vlnv xilinx.com:hls:tcp_in:1.0 tcp_in_0
create_bd_cell -type ip -vlnv xilinx.com:hls:tcp_out:1.0 tcp_out_0

create_bd_cell -type ip -vlnv user.org:user:cycle_counter_128:1.0 cycle_counter_128_0

create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 net_rst_in

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 pause_c

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 count_s_c

	##~~ 
	#
	create_bd_cell -type hier network_module
                                        	
	current_bd_instance network_module      

	create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_rx
	create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 s_axis_tx

	create_bd_intf_pin -mode Slave -vlnv xilinx.com:display_xxv_ethernet:gt_ports:2.0 gt_rx
	create_bd_intf_pin -mode Master -vlnv xilinx.com:display_xxv_ethernet:gt_ports:2.0 gt_tx

	create_bd_pin -dir I -type rst axis_aresetn
	create_bd_pin -dir I -type rst network_reset

	create_bd_pin -dir I -type clock dlck_100MHz
	create_bd_pin -dir I -type clock refclk_n
	create_bd_pin -dir I -type clock refclk_p
	create_bd_pin -dir O net_init_done
	create_bd_pin -dir O clk_161

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 vcc

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 gnd

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 seed_c

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 max_pack_l

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 min_pack_l

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 transc_clk_sel

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 loop_c

	create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 user_reset_and

	create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 reset_inv

	create_bd_cell -type ip -vlnv xilinx.com:ip:xxv_ethernet:3.0 xxv_ethernet_0

	create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_in

	create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_out

	create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 tx_pre_c

	create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 fifo_generator_0
	#
	##~~

current_bd_instance

create_bd_intf_port -mode Slave -vlnv xilinx.com:display_xxv_ethernet:gt_ports:2.0 gt_rx
create_bd_intf_port -mode Master -vlnv xilinx.com:display_xxv_ethernet:gt_ports:2.0 gt_tx

set logic_reset_op [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic logic_reset_op ]

startgroup
create_bd_cell -type ip -vlnv xilinx.com:hls:parser_arbiter:1.0 parse_arb_A
endgroup

startgroup
>> rep 1 create_bd_cell -type ip -vlnv xilinx.com:hls:parser:1.0 parse_X
endgroup

startgroup
>> rep 1 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 num_channels_c_X
endgroup

startgroup
>> rep2 1 10 create_bd_cell -type ip -vlnv user.org:user:enable_logic:3.0 enable_logic_X_Y
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 size_limit_X
>> rep2 1 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 enable_const_X_Y
>> rep2 10 1 set ready_in_w_or_X_Y [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 ready_in_w_or_X_Y ]
>> rep2 1 10 set read_req_wrtr_or_X_Y [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 read_req_wrtr_or_X_Y ]
endgroup

startgroup
>> rep 1 set channel_mask_X     [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 channel_mask_X ]
>> rep 1 set channel_mask_and_X [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 channel_mask_and_X ]
endgroup

startgroup
>> rep 1 create_bd_cell -type ip -vlnv xilinx.com:hls:hash_arbiter:1.0 hash_arb_X
>> rep 1 create_bd_cell -type ip -vlnv xilinx.com:hls:hash_arbiter:1.0 channel_arb_X
>> rep2 1 2 create_bd_cell -type ip -vlnv xilinx.com:hls:murmur3_f:1.0 hash_gen_X_Y
>> rep2 1 2 create_bd_cell -type ip -vlnv user.org:user:hash_rd:1.0 hash_rd_X_Y
>> rep2 1 3 create_bd_cell -type ip -vlnv xilinx.com:hls:murmur3_f:1.0 channel_sel_X_Y
>> rep2 1 3 create_bd_cell -type ip -vlnv user.org:user:hash_reducer:1.0 hash_reducer_X_Y
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 reducer_mode_c
>> rep2 1 2 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 seed_c_X_Y
>> rep2 1 3 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 seed_ch_c_X_Y
endgroup

startgroup
>> rep2 10 4 create_bd_cell -type ip -vlnv xilinx.com:hls:read_req_writer:1.0 read_req_writer_X_Y
#>> rep 10 set xcon_bram_read_X [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect xcon_bram_read_X ]
>> rep 10 set xcon_bram_read_X [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_bram_read_X ]
endgroup

startgroup
>> rep2 10 5 create_bd_cell -type ip -vlnv xilinx.com:hls:register_writer:1.0 reg_writer_X_Y
>> rep2 10 5 create_bd_cell -type ip -vlnv xilinx.com:hls:big_request_writer:1.0 big_request_writer_X_Y
#>> rep 10 set xcon_bram_write_X [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect xcon_bram_write_X ]
>> rep 10 set xcon_bram_write_X [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_bram_write_X ]
endgroup

startgroup
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:hls:register_arbiter:1.0 reg_arb_W_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:hls:register_arbiter:1.0 reg_arb_BW_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:hls:register_arbiter:1.0 reg_arb_R_X
endgroup

startgroup
>> rep2 10 6 create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_read_ctrl_X_Y
>> rep2 10 6 set bram_read_X_Y [create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 bram_read_X_Y]
endgroup

startgroup
>> rep2 10 7 create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_write_ctrl_X_Y
>> rep2 10 7 set bram_write_X_Y [create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 bram_write_X_Y]
endgroup

startgroup
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 mem_base_address_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:hls:memory_writer:1.0 memory_writer_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:hls:memory_reader:1.0 memory_reader_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 register_offset_X
#create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_memory_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_write_mem_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_big_mem_A
#create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_read_mem_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1  xcon_read_mem_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_memory_A
endgroup

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_memory_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_memory_1


startgroup
set inter_axi_group [list\
>> rep 10 $xcon_bram_read_X\
>> rep 10 $xcon_bram_write_X\
]
endgroup

startgroup
set bram_grp [list\
>> rep2 10 6 $bram_read_X_Y\
>> rep2 10 7 $bram_write_X_Y\
]
endgroup

startgroup
>> rep2 1 10 create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 reg_arb_R_and_X_Y
>> rep2 1 10 create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 reg_arb_W_and_X_Y
>> rep2 1 10 create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 reg_arb_BW_and_X_Y
endgroup

startgroup
>> rep2 1 10 create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 parser_rw_not_X_Y
endgroup

startgroup
set hbm_inst [ create_bd_cell -type ip -vlnv xilinx.com:ip:hbm:1.0 hbm_inst ]
endgroup

startgroup
set refclk_ibufds [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 refclk_ibufds ]
set refclk_bufg [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 refclk_bufg ]
endgroup

startgroup
set zero1_inst [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 zero1_inst ]
set zero22_inst [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 zero22_inst ]
set zero32_inst [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 zero32_inst ]
endgroup

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_in_req_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_in_write_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_in_read_A
#create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_in_read_A
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_in_big_A
>> repMul2 create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_req_ctrl_X
>> repMul2 create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 in_req_bram_X

create_bd_cell -type ip -vlnv user.org:user:cycle_counter_128:1.0 cycle_counter_0
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 count_start_c
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 counter_xor_c
create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 counter_xor
create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 counter_inv
create_bd_cell -type ip -vlnv xilinx.com:ip:util_reduced_logic:2.0 xor_reduce

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_tcp_mem

>> rep_hl 0 create_bd_cell -type ip -vlnv xilinx.com:hls:request_handler:1.0 request_handler_X

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 req_h_mem_b
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 req_h_mem_m
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 mem_size_b
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 mem_size_m
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 num_mem_c
>> rep/2 0 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 handler_num_b_X
>> rep/2 0 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 handler_num_m_X

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_tcp_bram_A
>> rep 8 create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_tcp_ctrl_X
>> rep 8 create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 tcp_bram_X

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 xcon_mem_temp_A

>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 req_base_w_c_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 req_base_b_c_X
>> rep 10 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 req_base_r_c_X

create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 mem_calib_and

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 tmp_mems_b_c
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 tmp_mems_r_c
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 tmp_mems_w_c

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 BRAM_size_c

>> rep2 10 5 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 own_big_w_c_X_Y
>> rep2 10 5 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 own_mem_r_c_X_Y
>> rep2 10 5 create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 own_mem_w_c_X_Y
