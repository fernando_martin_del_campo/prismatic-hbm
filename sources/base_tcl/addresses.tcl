# Create address segments

>> rep2 10 6 create_bd_addr_seg -range RANGE_R -offset BASE_R [get_bd_addr_spaces memory_reader_X/Data_M_AXI_BUS_READ_REG] [get_bd_addr_segs axi_bram_read_ctrl_X_Y/S_AXI/Mem0] SEG_axi_bram_read_ctrl_X_Y_Mem0

>> rep2 10 7 create_bd_addr_seg -range RANGE_W -offset BASE_W [get_bd_addr_spaces memory_writer_X/Data_M_AXI_BUS_READ] [get_bd_addr_segs axi_bram_write_ctrl_X_Y/S_AXI/Mem0] SEG_axi_bram_write_ctrl_X_Y_Mem0

>> rep3 10 4 6 create_bd_addr_seg -range RANGE_R -offset BASE_R [get_bd_addr_spaces read_req_writer_X/Data_M_AXI_BUS_IF] [get_bd_addr_segs axi_bram_read_ctrl_X_Z/S_AXI/Mem0] SEG_axi_bram_read_ctrl_X_Z_Mem0

>> rep3 10 5 7 create_bd_addr_seg -range RANGE_W -offset BASE_W [get_bd_addr_spaces reg_writer_X/Data_M_AXI_BUS_IF] [get_bd_addr_segs axi_bram_write_ctrl_X_Z/S_AXI/Mem0] SEG_axi_bram_write_ctrl_X_Z_Mem0

#>> rep2 10 5 create_bd_addr_seg -range RANGE_M -offset BASE_M [get_bd_addr_spaces big_request_writer_X/Data_M_AXI_BUS_WRITE] [get_bd_addr_segs hbm_inst/SAXI_0Z/HBM_MEM0Z] SEG_hbm_inst_HBM_MEM0Z

#>> rep 10 create_bd_addr_seg -range RANGE_M -offset BASE_M [get_bd_addr_spaces memory_reader_X/Data_M_AXI_BUS_READ_MEM] [get_bd_addr_segs hbm_inst/SAXI_0Z/HBM_MEM0Z] SEG_hbm_inst_HBM_MEM0Z

#>> rep 10 create_bd_addr_seg -range RANGE_M -offset BASE_M [get_bd_addr_spaces memory_writer_X/Data_M_AXI_BUS_WRITE] [get_bd_addr_segs hbm_inst/SAXI_0Z/HBM_MEM0Z] SEG_hbm_inst_HBM_MEM0Z

create_bd_addr_seg -range 0x20000000 -offset 0xE0000000 [get_bd_addr_spaces TCP_Subsystem/tcp_ip_wrapper_0/M00_AXI] [get_bd_addr_segs hbm_inst/SAXI_14/HBM_MEM14] SEG_hbm_inst_HBM_MEM14

>> rep2lh 0 8 create_bd_addr_seg -range RANGE_TCP -offset BASE_TCP [get_bd_addr_spaces request_handler_X/Data_M_AXI_REQ_IN] [get_bd_addr_segs axi_bram_tcp_ctrl_Z/S_AXI/Mem0] SEG_axi_bram_tcp_ctrl_Z_Mem0

>> rep_p 8 create_bd_addr_seg -range RANGE_TCP -offset BASE_TCP [get_bd_addr_spaces TCP_Subsystem/tcp_in_0/Data_m_axi_Data_out_V] [get_bd_addr_segs axi_bram_tcp_ctrl_X/S_AXI/Mem0] SEG_axi_bram_tcp_ctrl_X_Mem0

create_bd_addr_seg -range 0x10000000 -offset 0xF0000000 [get_bd_addr_spaces TCP_Subsystem/tcp_in_0/Data_m_axi_Data_out_big_V] [get_bd_addr_segs hbm_inst/SAXI_15/HBM_MEM15] SEG_hbm_inst_HBM_MEM15
#create_bd_addr_seg -range 0x10000000 -offset 0x00000000 [get_bd_addr_spaces TCP_Subsystem/tcp_out_0/Data_m_axi_Data_in_mem_V] [get_bd_addr_segs hbm_inst/SAXI_00/HBM_MEM00] SEG_hbm_inst_HBM_MEM15
>> rep2uh 0 create_bd_addr_seg -range 0x10000000 -offset 0xF0000000 [get_bd_addr_spaces request_handler_X/Data_M_AXI_REQ_IN] [get_bd_addr_segs hbm_inst/SAXI_15/HBM_MEM15] SEG_hbm_inst_HBM_MEM15

>> repMul2 assign_bd_address [get_bd_addr_segs {axi_bram_req_ctrl_X/S_AXI/Mem0 }] -offset BASE_BRAM -range RANGE_W

>> rep 10 assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_0Z/HBM_MEM0Z }] -offset BASE_M -range RANGE_M

assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_01/HBM_MEM01 }] -offset 0x10000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_02/HBM_MEM02 }] -offset 0x20000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_03/HBM_MEM03 }] -offset 0x30000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_04/HBM_MEM04 }] -offset 0x40000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_05/HBM_MEM05 }] -offset 0x50000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_06/HBM_MEM06 }] -offset 0x60000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_07/HBM_MEM07 }] -offset 0x70000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_08/HBM_MEM08 }] -offset 0x80000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_09/HBM_MEM09 }] -offset 0x90000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_10/HBM_MEM10 }] -offset 0xA0000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_11/HBM_MEM11 }] -offset 0xB0000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_12/HBM_MEM12 }] -offset 0xC0000000 -range 0x10000000
assign_bd_address [get_bd_addr_segs {hbm_inst/SAXI_13/HBM_MEM13 }] -offset 0xD0000000 -range 0x10000000

