#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

volatile unsigned int * gpio_b = (unsigned int *)0x40000000;
volatile unsigned int * gpio2_b = (unsigned int *)0x40000008;
volatile unsigned int * ddr_b  = (unsigned int *)0x80000000;
volatile unsigned int * ddr_b2  = (unsigned int *)0xA0000000;

volatile unsigned int * ddr_last  = (unsigned int *)0xE239A3C8;

#define xprint xil_printf
unsigned int index_mem;

int main()
{
    init_platform();

    //xprint("Hello World\n\r");
    /*while(1)
    {
      if ( *gpio2_b == 1)
         break;
    }*/

    xprint("Clearing the memory ...\n\r");
    for(index_mem=0; index_mem<0x8000000; index_mem++)
    {
        *(ddr_b+index_mem) = 0;
        *(ddr_b2+index_mem) = 0;
    }

    xprint("Done. Starting the system ...\n\r");
    *gpio2_b = 1;
    xprint("Done. System running ...\n\r");
    while(1)
    {
    	xprint("Req %u ...\n\r",*gpio_b);
        if ( *gpio_b == 41)
         break;
    }

    xprint("System done. Reading non-zero entries ...\n\r");

    for(index_mem=0; index_mem<0x8000000; index_mem++)
    {
       if ( *(ddr_b+index_mem) != 0)
           xprint("Mem[%X]: %08x\n\r", 0x80000000 + (index_mem)*4,*(ddr_b+index_mem));
    }
    //xprint("Mem[L]: %08x\n\r", *ddr_last);

    xprint("\n\r###########################################################\n\r");

    for(index_mem=0; index_mem<0x8000000; index_mem++)
    {
       if ( *(ddr_b2+index_mem) != 0)
          xprint("Mem[%X]: %08x\n\r", 0xA0000000 + (index_mem)*4,*(ddr_b2+index_mem));
    }

    xprint("Memory checked\n\r");

    cleanup_platform();
    return 0;
}
